<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manpower RightEverywhere </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/icon-style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="profilePage" class="dashboard">
  <?php include 'includes/head.php' ;?>

    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
      <!-- SIDEBAR MENU START -->
      <?php include 'includes/sidebar.php' ;?>
      <!-- SIDEBAR MENU END -->


      <!-- CONTAINER WRAP START -->
      <div class="container_wrp">
        <div class="dash_wrp">
          <div class="row">
            <h1>Profile</h1>
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <h4 class="grayTitle">Personal Info <a id="edit_name"  href="#"> <i class="glyphicon glyphicon-edit right">edit</i></a> <a id="save_name" href="#"> <i class="glyphicon glyphicon-floppy-save right">Save</i></a></h4>
              <div class="right col-xs-12 col-sm-12 col-md-12 personal-info">
                <div class="row">
                  <div class="mr4 col-xs-12 col-sm-5 col-lg-5">
                    <div class="form-group has-feedback">
                      <label for="firstName">First Name</label>
                      <div class="fieldValue profile_f_pad">Darell</div>
                      <input type="text" class="form-control hiddenInput" id="firstName" name="fname" placeholder="Darell">
                    </div>
                    <div class="form-group has-feedback">
                      <label for="lastName">Last Name</label>
                      <div class="fieldValue profile_f_pad">Jackson</div>
                      <input type="text" class="form-control hiddenInput" id="lastName" name="lname" placeholder="Jackson">
                    </div>
                    <div class="form-group has-feedback">
                      <label for="emailAddress">Email Address</label><br>
                      <div class="fieldValue profile_f_pad">email@website.com</div>
                      <input type="text" class="form-control hiddenInput" placeholder="email@website.com" id="emailAddress" name="eAddress">
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-5 col-lg-5">
                    <div class="form-group has-feedback">
                      <label for="emailAddress">Phone</label>
                      <div class="fieldValue profile_f_pad">123-123-1234</div>
                      <input type="text" class="form-control hiddenInput" placeholder="123-123-1234" id="emailAddress" name="eAddress">
                    </div>
                    <label for="pLanguage">Preferred Language</label>
                    <select class="regLanguage" name="pLanguage" id="pLanguage">
                      <option value="english">English</option>
                      <option value="spanish">Spanish</option>
                      <option value="sample">Sample</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="dash_container col-xs-12 col-sm-12 col-lg-12">
              <h4 class="grayTitle">Password Info</h4>
              <div class="col-xs-12 col-sm-12 col-lg-12 pwordProfile">
                <div class="form-group has-feedback">
                  <label for="password">Current Password</label>
                  <input type="text" class="form-control" id="password" name="password">
                </div>
                <div class="form-group has-feedback">
                  <label for="newpassword">New Password</label>
                  <input type="text" class="form-control" id="newPassword" name="newPassword">
                </div>
                <div class="form-group has-feedback">
                  <label for="ConfirmPassword">Confirm Password</label>
                  <input type="text" class="form-control" id="ConfirmPassword" name="ConfirmPassword">
                </div>
                <div class="reg_bntWRP">
                  <input class="continue contBTN col-xs-12 col-sm-8 col-lg-8" type="button" value="Save Changes" style="margin:2em 0;">
                </div>
              </div>
            </div>
            <div class="row">
        <div class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left">
          <h5>ASSESSMENT RESULTS</h5>
          <div class="p2">
            <ul class="bullet2">
              <li><a href="#">Determine Value And Drive</a></li>
              <li class="incomplete"><a href="#">Complete thought provoking statements</a></li>
              <li><a href="#">Analyze your strengths</a></li>
              <li><a href="#">Identify your ideal position</a></li>
              <li class="incomplete"><a href="#">Look at options</a></li>
              <li><a href="#">Resume Builder</a></li>
              <li class="incomplete"><a href="#">Prepare for a job search</a></li>
              <li class="incomplete"><a href="#">Prepare for an interview</a></li>
              <li class="incomplete"><a href="#">Evaluate an offer</a></li>
            </ul>
          </div>
        </div>
        <div class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right">
          <h5>MY PURCHASES</h5>
          <div class="p2">
              <ul class="bullet2">
              <li><a href="#">PURCHASE 1</a></li>
              <li><a href="#">PURCHASE 2</a></li>
              <li><a href="#">PURCHASE 3</a></li>
              <li><a href="#">PURCHASE 4</a></li>
            </ul>
            <a class="upload">VIEW ALL PREVIOUS PURCHASES</a>
          </div>
        </div>
      </div>

    </div><!--/.row-->
   </div>
 </div>
    <?php include 'includes/footer.php' ;?>
    </div><!-- END MAIN WRAPPER -->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="js/modernizr.custom.js"></script>
    <script src="js/dashboard.js"></script>
    <script>
      $(function() {
        $("#formName").hide();
        $("#save_name").hide();
        $(".hiddenInput").hide();
        $(".hiddenTest").hide();
      });
      $("#edit_name").click(function(){
        $("#displayName").fadeTo( "fast" , 0) .hide();
        $("#formName").fadeTo( "fast" , 1);
        $(".fieldValue").fadeTo( "fast" , 0) .hide();
        $(".hiddenInput").fadeTo( "fast" , 1);
        $(".hiddenTest").show();
        $("#save_name").show();
        $("#edit_name").hide();
      });
      $("#save_name").click(function(){
        $("#formName").fadeTo( "fast" , 0) .hide();
        $("#displayName").fadeTo( "fast" , 1) ;
        $(".hiddenInput").fadeTo( "fast" , 0).hide();
        $(".fieldValue").fadeTo( "fast" , 1);
        $(".hiddenTest").hide();
        $("#save_name").hide();
        $("#edit_name").show();
      });
     </script>


  </body>
  </html>
