<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Manpower RightEverywhere</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/icon-style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="dashboardPage" class="dashboard">
     <div class="header_wrp navbar navbar-default"  role="navigation">
      <a  href="#menu-toggle" id="menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </a>
      <div class="dash_logo">
        <img id="logo" src="images/right_logo.png" alt="Right Mangement Logo" />
      </div>
      
      <ul class="header_nav hidden-xs">
        <li><a href="#">Resources</a></li>
        <li><a href="#">Log Out</a></li>
      </ul>
      <a href="#" class="hidden-xs" id="messages">
        <span>3</span>
      </a>
    </div>  
    <!-- MODAL DEMO SECTION  
     <div id="myModal" class="modal fade">
    <div class="modal-dialog modal-vertical-centered">
        <div class="modal-content">
           
                
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                
              <h4 class="modal-title">Welcome to RightEverywhere.</h4>
                Stay &amp; Grow. Supporting employees and their manager al al levels through a process of discovery to help them clarify their career value proposition and needs, and the align these with the organization to drive their career, thereby increasing alignment, engagement and productivity.
            </div>
        </div>
    </div> 
</div>
-->
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
       <!-- SIDEBAR MENU START -->
      <div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
        <a href="#clost-menu-toggle" class="close-toggle" id="close-menu-toggle">X</a>
        <ul class="nav top-nav hidden-sm hidden-md hidden-lg">
          <li><a class="noICO" href="#">NOTIFICATIONS</a></li>
          <li><a class="noICO" href="#">RESOURCES</a></li>
          <li><a class="noICO" href="#">LOG OUT</a>
        </ul>
        <ul class="nav orange-icon" id="orange_icon">
          <li><div class="navTitle">MAIN</div></li>
          <li><a class="active" href="#"><span class="icon-dashboard-icon icon-md nav-color" aria-hidden="true"></span>Dashboard</a></li>
          <li><a href="#"><span class="icon-profile-icon icon-md nav-color" aria-hidden="true"></span>Profile</a></li>
          <li><a class="carrer_coach" href="#"><span class="icon-career-coach-icon icon-md nav-color" aria-hidden="true"></span>Career Coach</a>
            <ul>
              <li id="coach-info">
                <h4>Han Solo</h4>
                <div class="phone">123-123-1234</div> 
                <div class="email">hansolo@galaxy.com</div>
              </li>
            </ul>
          </li>
          <li><a href="#"><span class="icon-favorite-icon icon-md nav-color" aria-hidden="true"></span>Favorites</a></li> 
          <li><a href="#"><span class="icon-icon_options icon-md nav-color" aria-hidden="true"></span>Options</a></li>
                    
        </ul>
        <div class="sb-footer navbar hidden-sm hidden-md hidden-lg">
          <ul class="footerNav "> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </div>
      <!-- SIDEBAR MENU END -->
      
      <!-- CONTAINER WRAP START -->
      <div class="container_wrp"> 


        <!-- HEADER BANNER START -->
        <div id="welcome_banner" class="milwaukee_morn">
          <h2 class="message">Good Morning Darrell</h2>
          <div class="location">Milwaukee, WI</div>
          <div class="date">July, 29 2014</div> 
        </div>
        <!-- HOME BANNER END -->

        <!-- DASH WRAP START -->
        <div class="dash_wrp">
          <div class="row"> 
            
            <div class="dash_container ">
              
              <!-- DASHBOARD PROGRESS -->
              <div class="col-xs-12 col-sm-12 col-md-12 right overall_prog ">
                <h4 class="p0_5">Continuous Connections</h4>
                <p class="p0-1">Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. Eu autem invidunt neglegentur pro, iusto audire platonem in nec.</p>
                <p class="p0-1"><a heref="#">Got a job offer? Get 30 minutes of free coaching</a></p>
            
              </div>
              <!-- DASHBOARD PROGRESS END -->
            </div> 
            
            <div class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left moreBtnSM-container">
             <div class="container_heading_sm">RESOURCES<div class="icon-resources-icon icon-md right orange_txt" aria-hidden="true"></div></div>
              <div class="p2">
                <div class="sm-job-research">
                  <h6>Resource One</h6>
                  This phase of Life Options is all about discovery - discovering what will form the foundation of your plan for achieving the kind of life that you would consider most fulfilling.
                </div>
               
                <div class="sm-job-research">
                  <h6>Resource Two</h6>
                  This phase of Life Options is all about discovery - discovering what will form the foundation of your plan for achieving the kind of life that you would consider most fulfilling.
                </div>
               <div class="moreBtnSm"><a href="#" class="orange button">SEE MORE RESOURCES</a></div>
              </div>
            </div>

            <div class="dash_container  col-xs-12 col-sm-5-5 col-md-5-5 right">
              <div class="container_heading_sm">JOB SEARCH: <span>Job Openings on Right Jobs</span><div class="icon-job-openings-icon icon-lg right orange_txt" aria-hidden="true"></div></div>
              <div class="gray-heading">
                <div class="col-md-8">JOBS</div>
              </div>
              <ul id="job-search-list">
                <li class="job-post">
                  <a href="#">
                  <div class="col-md-12"><span class="job-title">Position Title</span><span class="comp-title">Company name</span></div>
                  </a>
                </li> 
                <li class="job-post">
                  <a href="#">
                  <div class="col-md-12"><span class="job-title">Position Title</span><span class="comp-title">Company name</span></div>
                  </a>
                </li>
                <li class="job-post">
                  <a href="#">
                  <div class="col-md-12"><span class="job-title">Position Title</span><span class="comp-title">Company name</span></div>
                  </a>
                </li>
              </ul>
            </div>
           

           
 <!-- START BLOG WRAP -->           
            
              <div class="dash_container col-xs-12 col-sm-7-5 col-md-7-5 right">
                <h5 class="gray">Career + Work Blog</h5>
                <div class="row p2">
                  <!-- BLOG COLUMN ONE -->
                  <div class="col-xs-12 col-sm-12 col-md-6"> 
                    <div class="resource_blogWrp">
                      <a class="title" href="#">Blog Title</a>
                      <div class="excerpt">
                        Job search and yoga at first seem like an unlikely pairing, but there are many lessons learned from yoga that can...
                        <a href="#">View the [Blog Name] <i class="glyphicon glyphicon-chevron-right"></i></a>
                      </div>
                    </div>
                    <div class="resource_blogWrp">
                      <a class="title" href="#">Blog Title</a>
                      <div class="excerpt">
                        Job search and yoga at first seem like an unlikely pairing, but there are many lessons learned from yoga that can...
                        <a href="#">View the [Blog Name] <i class="glyphicon glyphicon-chevron-right"></i></a>
                      </div>
                    </div>   
                  </div><!-- END BLOG COLUMN ONE -->
                  <!-- BLOG COLUMN TWO -->
                  <div class="col-xs-12 col-sm-12 col-md-6"> 
                    <div class="resource_blogWrp">
                      <a class="title" href="#">Blog Title</a>
                      <div class="excerpt">
                        Job search and yoga at first seem like an unlikely pairing, but there are many lessons learned from yoga that can...
                        <a href="#">View the [Blog Name] <i class="glyphicon glyphicon-chevron-right"></i></a>
                      </div>
                    </div>
                    <div class="resource_blogWrp">
                      <a class="title" href="#">Blog Title</a>
                      <div class="excerpt">
                        Job search and yoga at first seem like an unlikely pairing, but there are many lessons learned from yoga that can...
                        <a href="#">View the [Blog Name] <i class="glyphicon glyphicon-chevron-right"></i></a>
                      </div>
                    </div>
                  </div><!-- END BLOG COLUMN TWO -->
                  
                </div><!-- END ROW -->
              </div><!-- END DASH CONTAINER -->
            <!-- END BLOG WRAP --> 

           
             <!-- START DASHBOARD CAREER COACH -->
            <div class="dash_container col-xs-12 col-sm-4 col-md-4 left">
              <h5>Connect With Us</h5>
              <div class="p2" id="dash-social">
                <div class="list-link"><a href="#">Facebook <i class="glyphicon glyphicon-chevron-right"></i></a></div>
                <div class="list-link"><a href="#">Twitter <i class="glyphicon glyphicon-chevron-right"></i></a></div>
                <div class="list-link"><a href="#">Linkedin <i class="glyphicon glyphicon-chevron-right"></i></a></div>
              </div>
            </div>
            <!-- END DASHBOARD CAREER COACH --> 

          </div><!-- END ROW -->
        </div><!-- END DASH WRAP -->
      </div><!-- END CONTAINER WRAP -->

      <footer class="dashfooter navbar hidden-xs">
      <!-- INCLUDE FOOTER IN FINAL CODE  < ?php include 'includes/footer.php' ;?> -->
        <div class="inner-footer">
          <div class="footer-logo">
            <img id="ft-logo" src="images/logo.png" alt="Right Mangement Logo" />
          </div>
          <ul class="footerNav"> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </footer> 
    </div><!-- END MAIN WRAPPER --> 
  
    
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
   
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   

      <script src="js/circle-progress.js"></script>
      <script src="js/modernizr.custom.js"></script>
     
     <script>
        // SVG LOGO DETECT // 
        if (Modernizr.svg)
        {
          $("#logo").attr("src", "images/right_logo.svg");
          //console.log('changed');

          $("#ft-logo").attr("src", "images/logo.svg");
          //console.log('changed');
        }; 
        
        
        // WELCOME MODAL // 
       
       function reposition() {
        var modal = $(this),
            dialog = modal.find('.modal-vertical-centered');
        modal.css('display', 'none'); // SET DISPLAY TO BLOCK TO ACTIVATE MODAL ON PAGE LOAD
        
        // Dividing by two centers the modal exactly, but dividing by three 
        // or four works better for larger screens.
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
        }
        // Reposition when a modal is shown
        $('.modal').on('show.bs.modal', reposition);
        // Reposition when the window is resized
        $(window).on('resize', function() {
            $('.modal:visible').each(reposition);
        });


        $(window).load(function(){
        $('#myModal').modal('show');
        });
       
        // MOBILE SIDE MENU TOGGLE // 
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

         $("#close-menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").removeClass("toggled");
        });
        
        // HIDES MOBILE SUB MENU //
        $(".carrer_coach").click(function(e) {
            e.preventDefault();
            $("#coach-info").toggleClass("invisible");
        });

        $(".help").click(function(e) {
            e.preventDefault();
            $("#help-info").toggleClass("invisible");
        });

        // ANIMATES DASHBOARD PROGRESS CIRCLE // 
        $('.progressCircle').circleProgress({
            value: 0.45,
            size: 220,
            startAngle: 4.7,
            fill: { gradient: ['#4a94eb', '#5d9c9a'] }
        }).on('circle-animation-progress', function(event, progress) {
            $(this).find('span').html(parseInt(45 * progress) + '<i>%</i>');
        });
     </script>
  </body>
</html>
