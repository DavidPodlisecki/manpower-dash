<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Manpower RightEverywhere </title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/icon-style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="resumePage" class="dashboard">
    <!-- START HEADER -->
    <div class="header_wrp navbar navbar-default"  role="navigation">
      <a  href="#menu-toggle" id="menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </a>
      <div class="dash_logo">
        <img id="logo" src="../images/right_logo.png" alt="Right Mangement Logo" />
      </div>
      
      <ul class="header_nav hidden-xs">
        <li><a href="#" >Resources</a></li>
        <li><a href="#">Log Out</a></li>
      </ul>
      <a href="#" class="hidden-xs" id="messages">
        <span>3</span>
      </a>
    </div>
    <!-- END HEADER -->

    <!-- START WRAPPER -->
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
      <!-- SIDEBAR MENU START -->
      <div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
        <a href="#clost-menu-toggle" class="close-toggle" id="close-menu-toggle">X</a>
        <ul class="nav top-nav hidden-sm hidden-md hidden-lg">
          <li><a class="noICO" href="#">NOTIFICATIONS</a></li>
          <li><a class="noICO" href="#">RESOURCES</a></li>
          <li><a class="noICO" href="#">LOG OUT</a>
        </ul>
        <ul class="nav orange-icon" id="orange_icon">
          <li><div class="navTitle">MAIN</div></li>
          <li><a href="#"><span class="icon-dashboard-icon icon-md nav-color" aria-hidden="true"></span>Dashboard</a></li>
          <li><a href="#"><span class="icon-profile-icon icon-md nav-color" aria-hidden="true"></span>Profile</a></li>
          <li><a class="carrer_coach" href="#"><span class="icon-career-coach-icon icon-md nav-color" aria-hidden="true"></span>Career Coach</a>
            <ul>
              <li id="coach-info">
                <h4>Han Solo</h4>
                <div class="phone">123-123-1234</div> 
                <div class="email">hansolo@galaxy.com</div>
              </li>
            </ul>
          </li>
          <li><a href="#"><span class="icon-events-icon icon-md nav-color" aria-hidden="true"></span>Events &amp; Webinars</a></li>
          <li><a href="#"><span class="icon-favorite-icon icon-md nav-color" aria-hidden="true"></span>Favorites</a></li> 
          <li><a class="help" href="#"><span class="icon-help-icon icon-md nav-color" aria-hidden="true"></span>Help</a>
            <ul>
              <li id="help-info" class="help-desk">
                <h4>Help Desk</h4>
                <div class="p2">
                  <p>For assistance with RightEverywhere technical questions and quick job search coaching questions, please call <strong>800.668.8556</strong> </p>
                  <p>8AM- 8PM Eastern (Mon - Fri) <br>9AM- 5PM Eastern (Sat - Sun)</p>
                </div>
              </li>
            </ul>
          </li>
          <li><div class="navTitle">PROGRESS</div></li>
          <li><a href="#"><span class="icon-discovery-icon icon-md nav-color" aria-hidden="true"></span>Self Discovery</a></li>
          <li><a href="#"><span class="icon-career-planning-icon icon-md nav-color" aria-hidden="true"></span>Career Planning</a></li>
          <li><a class="active" href="#"><span class="icon-personal-branding-icon icon-md nav-color" aria-hidden="true"></span>Personal Branding</a></li>
          <li><a href="#"><span class="icon-job-search-icon icon-md nav-color" aria-hidden="true"></span>Job Search</a></li>
          <li><a href="#"><span class="icon-close-deal-icon icon-md nav-color" aria-hidden="true"></span>Close the Deal</a></li>             
        </ul>
        <div class="sb-footer navbar hidden-sm hidden-md hidden-lg">
          <ul class="footerNav "> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </div>
      <!-- SIDEBAR MENU END -->
      
      
      <!-- START PAGE CONTAINER -->
      <div class="container_wrp" id="container_wrp">
        <div class="dash_wrp">
          <div class="row">
            <a href="#" class="left backBTN"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Back To Personal Branding</a>
            
            <!-- RESUME BUILDER --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <div class="p2">
                <h4 class="resource dkred_txt">Resume Builder</h4>
                <div class="right col-xs-12 col-sm-12 col-md-12">
                  <div class="row">
                    <h6 class="resource_list_title">Instructions</h6>
                    <p>
                      <ul>
                        <li>Follow the prompts to choose your format and input your work history. </li>
                        <li>Refer to resume examples as necessary. These will help you see the full picture of what you are creating.</li>
                        <li>You do not need to fill in every section. If something is not applicable to you, skip it and move to the next. </li>
                      </ul>
                    </p>
                  
                  </div> 
                  
                  <a class="green button right mb2" href="#">EXIT</a><a class="blue button right mb2" href="#">SAVE</a> 
                   
                </div>
              </div>
            </div><!-- END RESUME BUILDER --> 

            <h4 class="info_header" id="info_header1">Create a Resume</h4>

            <!-- CREATE A RESUME--> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <div class="p2">
                <div class="row">
                  <!-- form one -->
                  <form id="create" class="square_input">
                    <div class="col-xs-12 col-sm-12 col-lg-12">
                      <h6>Resume Name</h6>
                      <p>This name will be used to help manage multiple versions of your resume.</p>
                      <div class="form-group has-feedback col-xs-12 col-sm-7 col-lg-7 p0">
                        <label for="resumeName" class="hiddenLabel">Name</label>
                        <input type="text" class="form-control " id="resumeName" placeholder="Name" name="resumeName">
                      </div>
                      <span class="line"></span>
                      <h6 class="col-xs-12 col-sm-12 col-lg-12 p0">Select a format</h6>
                      <div class="resume_input_wrp">
                        <input type="radio" name="resume_type" value="chronological" checked >
                        <span class="blue_txt">CHRONOLOGICAL</span><br>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                          Chronological style resumes are the most effective format to use if you are targeting the same/similar type of position as your most recent position(s). The professional experience is listed in reverse chronology beginning with the most recent job.
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6 lt-gray">
                          Jobs that use this format are: ipsum dolor sit amet, consectetur adipiscing elit, sed do elusmond tempor incididunt ut labore.
                        </div>
                      </div>
                      <div class="resume_input_wrp">
                        <input type="radio" name="resume_type" value="functional">
                        <span class="blue_txt">FUNCTIONAL</span><br>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                          Functional or hybrid style resumes are effective if you are changing careers (targeting a new/different type of position) or targeting a consulting role. This format highlights “transferable” or functional skills and experience that are applicable to the new type of position or career. It focuses the reader’s attention and accomplishments that demonstrate key skills/expertise regardless of where or when they were used.
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6 lt-gray">
                          Jobs that use this format are: ipsum dolor sit amet, consectetur adipiscing elit, sed do elusmond tempor incididunt ut labore.
                        </div>
                      </div>
                      <div class="resume_input_wrp">
                        <input type="radio" name="resume_type" value="cv">
                        <span class="blue_txt">CV</span><br>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                          These are primarily used in academic and scientific fields. The initial ages resemble a typical chronological format, however the education appears frst and an addendum is added outling professional achievments such as patents, presentations, and publications. While a resume provides a concise snapshot of professional experience and accomplishments, the CV is more comprehensive.
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6 lt-gray">
                          Jobs that use this format are: ipsum dolor sit amet, consectetur adipiscing elit, sed do elusmond tempor incididunt ut labore.
                        </div>
                      </div>
                    </div> 
                  </form>  
                  <a class="orange button right next" id="next1"  href="resume_builder_step2.php">NEXT</a>
                   <!-- end form one -->
                </div> <!-- end row -->
              </div><!-- END P2 -->
            </div><!-- END CREATE A RESUME --> 
          </div><!-- END ROW -->
        </div><!-- END DASH WRAP -->
      </div><!-- END PAGE CONTAINER -->     
        
      <footer class="dashfooter navbar hidden-xs">
      <!-- INCLUDE FOOTER IN FINAL CODE  < ?php include 'includes/footer.php' ;?> -->
        <div class="inner-footer">
          <div class="footer-logo">
            <img id="ft-logo" src="../images/logo.png" alt="Right Mangement Logo" />
          </div>
          <ul class="footerNav"> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </footer> 
    </div><!-- END MAIN WRAPPER --> 
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   

      <script src="../js/modernizr.custom.js"></script>
     
    <script>
        // SVG LOGO DETECT // 
        if (Modernizr.svg)
        {
          $("#logo").attr("src", "../images/right_logo.svg");
          console.log('changed');

          $("#ft-logo").attr("src", "../images/logo.svg");
          console.log('changed');
        }; 

      

        // MOBILE SIDE MENU TOGGLE // 
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

         $("#close-menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").removeClass("toggled");
        });
        
        // HIDES MOBILE SUB MENU //
        $(".carrer_coach").click(function(e) {
            e.preventDefault();
            $("#coach-info").toggleClass("invisible");
        });

        $(".help").click(function(e) {
            e.preventDefault();
            $("#help-info").toggleClass("invisible");
        });

        
     </script>
    

  </body>
</html>
