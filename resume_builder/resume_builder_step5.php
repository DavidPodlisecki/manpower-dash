<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Manpower RightEverywhere </title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/icon-style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="notificationsPage" class="dashboard">
    <!-- START HEADER -->
    <div class="header_wrp navbar navbar-default"  role="navigation">
      <a  href="#menu-toggle" id="menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </a>
      <div class="dash_logo">
        <img id="logo" src="../images/right_logo.png" alt="Right Mangement Logo" />
      </div>
      
      <ul class="header_nav hidden-xs">
        <li><a href="#" >Resources</a></li>
        <li><a href="#">Log Out</a></li>
      </ul>
      <a href="#" class="hidden-xs" id="messages">
        <span>3</span>
      </a>
    </div>
    <!-- END HEADER -->

    <!-- START WRAPPER -->
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
      <!-- SIDEBAR MENU START -->
      <div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
        <a href="#clost-menu-toggle" class="close-toggle" id="close-menu-toggle">X</a>
        <ul class="nav top-nav hidden-sm hidden-md hidden-lg">
          <li><a class="noICO" href="#">NOTIFICATIONS</a></li>
          <li><a class="noICO" href="#">RESOURCES</a></li>
          <li><a class="noICO" href="#">LOG OUT</a>
        </ul>
        <ul class="nav orange-icon" id="orange_icon">
          <li><div class="navTitle">MAIN</div></li>
          <li><a href="#"><span class="icon-dashboard-icon icon-md nav-color" aria-hidden="true"></span>Dashboard</a></li>
          <li><a href="#"><span class="icon-profile-icon icon-md nav-color" aria-hidden="true"></span>Profile</a></li>
          <li><a class="carrer_coach" href="#"><span class="icon-career-coach-icon icon-md nav-color" aria-hidden="true"></span>Career Coach</a>
            <ul>
              <li id="coach-info">
                <h4>Han Solo</h4>
                <div class="phone">123-123-1234</div> 
                <div class="email">hansolo@galaxy.com</div>
              </li>
            </ul>
          </li>
          <li><a href="#"><span class="icon-events-icon icon-md nav-color" aria-hidden="true"></span>Events &amp; Webinars</a></li>
          <li><a href="#"><span class="icon-favorite-icon icon-md nav-color" aria-hidden="true"></span>Favorites</a></li> 
          <li><a class="help" href="#"><span class="icon-help-icon icon-md nav-color" aria-hidden="true"></span>Help</a>
            <ul>
              <li id="help-info" class="help-desk">
                <h4>Help Desk</h4>
                <div class="p2">
                  <p>For assistance with RightEverywhere technical questions and quick job search coaching questions, please call <strong>800.668.8556</strong> </p>
                  <p>8AM- 8PM Eastern (Mon - Fri) <br>9AM- 5PM Eastern (Sat - Sun)</p>
                </div>
              </li>
            </ul>
          </li>
          <li><div class="navTitle">PROGRESS</div></li>
          <li><a href="#"><span class="icon-discovery-icon icon-md nav-color" aria-hidden="true"></span>Self Discovery</a></li>
          <li><a href="#"><span class="icon-career-planning-icon icon-md nav-color" aria-hidden="true"></span>Career Planning</a></li>
          <li><a class="active" href="#"><span class="icon-personal-branding-icon icon-md nav-color" aria-hidden="true"></span>Personal Branding</a></li>
          <li><a href="#"><span class="icon-job-search-icon icon-md nav-color" aria-hidden="true"></span>Job Search</a></li>
          <li><a href="#"><span class="icon-close-deal-icon icon-md nav-color" aria-hidden="true"></span>Close the Deal</a></li>             
        </ul>
        <div class="sb-footer navbar hidden-sm hidden-md hidden-lg">
          <ul class="footerNav "> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </div>
      <!-- SIDEBAR MENU END -->
      
      <!-- START PAGE CONTAINER -->
      <div class="container_wrp" id="container_wrp">
        <div class="dash_wrp">
          <div class="row">
            <a href="#" class="left backBTN"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Back To Personal Branding</a>
            
            <!-- START RESOURCE --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <div class="p2">
                <h4 class="resource dkred_txt">Resume Builder</h4>
                <div class="right col-xs-12 col-sm-12 col-md-12">
                  <div class="row">
                    <h6 class="resource_list_title">Instructions</h6>
                    <p>
                      <ul>
                        <li>Follow the prompts to choose your format and input your work history. </li>
                        <li>Refer to resume examples as necessary. These will help you see the full picture of what you are creating.</li>
                        <li>You do not need to fill in every section. If something is not applicable to you, skip it and move to the next. </li>
                      </ul>
                    </p>
                  
                  </div> 
                  
                  <a class="green button right mb2" href="#">EXIT</a><a class="blue button right mb2" href="#">SAVE</a> 
                   
                </div>
              </div>
            </div><!-- END RESOURCE --> 

            <h4 class="info_header">Professional Experience</h4>
            

            <!-- START RESOURCE --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <div class="p2">
                <div class="row">
                  <!-- form three -->
                  <form id="experience" class="square_input">
                    <p>The Professional Experience section typically highlights the last 10-15 years of employment, however if prior experience builds your case stronger, then include it. Your experience substantiates the qualifications stated in the summary by allowing you to highlight your past roles, experiences and specific accomplishments. You should include at least one accomplishment for every year of employment that you present in your resume.</p>
                    <div class="col-xs-12 col-sm-6 col-lg-6 pl0">
                      <div class="form-group has-feedback">
                        <label for="employer">Employer</label>
                        <input type="text" class="form-control" id="employer" name="employer">
                      </div>
                      <div class="form-group has-feedback">
                        <label for="city">City</label>
                        <input type="text" class="form-control" id="city" name="city">
                      </div>
                      <div class="form-group has-feedback">
                        <label for="sdate">Start Date</label>
                        <input type="text" class="form-control" id="sdate" name="sdate">
                      </div>
                    </div> 
                    <div class="col-xs-12 col-sm-6 col-lg-6 pr0">
                      <div class="form-group has-feedback">
                        <label for="title">Position/Title</label>
                        <input type="text" class="form-control" id="title" name="title">
                      </div>
                       <div class="form-group has-feedback">
                        <label for="state">State</label>
                        <input type="text" class="form-control" id="state" name="state">
                      </div>
                      <div class="form-group has-feedback">
                        <label for="edate">End Date</label>
                        <input type="text" class="form-control" id="edate" name="edate">
                      </div>
                    </div> 
                    <p class="left mt1"><strong>Job Scope</strong> - give the reader a big-picture view of the work you completed in this position.<a href="#">See Example</a></p>
                    <div class="col-xs-12 col-sm-12 col-lg-12 p0">
                      <div class="form-group has-feedback">
                        <label for="jobscope" class="hiddenLabel">Job Scope</label>
                       <textarea name="jobscope" id="jobscope" class="col-xs-12 col-sm-12 col-lg-12" rows="10"> </textarea>
                      </div>
                    <p class="mt2-0 left"><strong>Job Accomplishments</strong> - Focus on your most significant accomplishments. Demonstrate the complexity of the situation and showcase your most impressive results. If you haven’t already, you may want to complete the Strengths Analysis before tackling this section of the resume<a href="#">See Example</a></p>
                    
                    <div class="col-xs-12 col-sm-7 col-lg-7 p0 ">
                      <p><input type="text" class="form-control" id="skill1" name="skill1"></p>
                      <p><input type="text" class="form-control" id="skill2" name="skill2"></p>
                      <p><input type="text" class="form-control" id="skill3" name="skill3"></p>
                      <p><input type="text" class="form-control" id="skill4" name="skill4"></p>
                      <p><input type="text" class="form-control" id="skill5" name="skill5"></p>
                      <p><input type="text" class="form-control" id="skill6" name="skill6"></p>
                      <p><input type="text" class="form-control" id="skill7" name="skill7"></p>
                      <a href="#" class="lt-gray">+ Add more filds</a>
                    </div>
                    <div class="p1 col-xs-12 col-sm-4 col-lg-4 right lt-gray-bx">
                      <span class="blue_txt ls1">FROM YOUR ASSESSMENTS</span><br>
                      <ul class="skill-list">
                        <li>Lorem ipsum dolor sit amet, censectetur <span class="green_txt">+add</span></li>
                        <li>Lorem ipsum dolor sit amet, censectetur <span class="green_txt">+add</span></li>
                        <li>Lorem ipsum dolor sit amet, censectetur <span class="green_txt">+add</span></li>
                        <li>Lorem ipsum dolor <span class="green_txt">+add</span></li>
                      </ul>
                    </div>
                  </form>
                  <div class="col-xs-12 col-sm-12 col-lg-12 p0"> 
                   <a class="left blue_txt ls1 mt2-0">+ ADD ANOTHER EXPERIENCE</a>
                   <a class="orange button right next mt2" id="next2"  href="resume_builder_step6.php">NEXT</a> <a class="orange_INV_link right next mt2" id="next2"  href="resume_builder_step4.php">PREVIOUS</a> 
                  </div>
                   <!-- end form three -->
                </div>
                    
                  
              </div>
            </div><!-- END RESOURCE --> 


             
          </div><!-- END ROW -->
        </div><!-- END DASH WRAP -->
      </div><!-- END PAGE CONTAINER -->     
        
      <footer class="dashfooter navbar hidden-xs">
      <!-- INCLUDE FOOTER IN FINAL CODE  < ?php include 'includes/footer.php' ;?> -->
        <div class="inner-footer">
          <div class="footer-logo">
            <img id="ft-logo" src="../images/logo.png" alt="Right Mangement Logo" />
          </div>
          <ul class="footerNav"> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </footer> 
    </div><!-- END MAIN WRAPPER --> 
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   

      <script src="../js/modernizr.custom.js"></script>
     
    <script>
        // SVG LOGO DETECT // 
        if (Modernizr.svg)
        {
          $("#logo").attr("src", "../images/right_logo.svg");
          console.log('changed');

          $("#ft-logo").attr("src", "../images/logo.svg");
          console.log('changed');
        };
      

    

        // MOBILE SIDE MENU TOGGLE // 
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

         $("#close-menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").removeClass("toggled");
        });
        
        // HIDES MOBILE SUB MENU //
        $(".carrer_coach").click(function(e) {
            e.preventDefault();
            $("#coach-info").toggleClass("invisible");
        });

        $(".help").click(function(e) {
            e.preventDefault();
            $("#help-info").toggleClass("invisible");
        });

        
     </script>
    

  </body>
</html>
