<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manpower RightEverywhere </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/icon-style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="dashboardPage" class="dashboard">
  <div class="header_wrp navbar navbar-default"  role="navigation">
      <a  href="#menu-toggle" id="menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </a>
      <div class="dash_logo">
        <img id="logo" src="images/right_logo.png" alt="Right Mangement Logo" />
      </div>
      
      <ul class="header_nav hidden-xs">
        <li><a href="#">Resources</a></li>
        <li><a href="#">Log Out</a></li>
      </ul>
      <a href="#" class="hidden-xs" id="messages">
        <span>3</span>
      </a>
    </div>    
    
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
      <!-- SIDEBAR MENU START -->
      <div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
        <a href="#clost-menu-toggle" class="close-toggle" id="close-menu-toggle">X</a>
        <ul class="nav top-nav hidden-sm hidden-md hidden-lg">
          <li><a class="noICO" href="#">NOTIFICATIONS</a></li>
          <li><a class="noICO" href="#">RESOURCES</a></li>
          <li><a class="noICO" href="#">LOG OUT</a>
        </ul>
        <ul class="nav orange-icon" id="orange_icon">
          <li><div class="navTitle">MAIN</div></li>
          <li><a href="#"><span class="icon-dashboard-icon icon-md nav-color" aria-hidden="true"></span>Dashboard</a></li>
          <li><a class="active" href="#"><span class="icon-profile-icon icon-md nav-color" aria-hidden="true"></span>Profile</a></li>
          <li><a class="carrer_coach" href="#"><span class="icon-career-coach-icon icon-md nav-color" aria-hidden="true"></span>Career Coach</a>
            <ul>
              <li id="coach-info">
                <h4>Han Solo</h4>
                <div class="phone">123-123-1234</div> 
                <div class="email">hansolo@galaxy.com</div>
              </li>
            </ul>
          </li>
          <li><a href="#"><span class="icon-events-icon icon-md nav-color" aria-hidden="true"></span>Events &amp; Webinars</a></li>
          <li><a href="#"><span class="icon-favorite-icon icon-md nav-color" aria-hidden="true"></span>Favorites</a></li> 
          <li><a class="help" href="#"><span class="icon-help-icon icon-md nav-color" aria-hidden="true"></span>Help</a>
            <ul>
              <li id="help-info" class="help-desk">
                <h4>Help Desk</h4>
                <div class="p2">
                  <p>For assistance with RightEverywhere technical questions and quick job search coaching questions, please call <strong>800.668.8556</strong> </p>
                  <p>8AM- 8PM Eastern (Mon - Fri) <br>9AM- 5PM Eastern (Sat - Sun)</p>
                </div>
              </li>
            </ul>
          </li>
          <li><div class="navTitle">PROGRESS</div></li>
          <li><a href="#"><span class="icon-discovery-icon icon-md nav-color" aria-hidden="true"></span>Self Discovery</a></li>
          <li><a href="#"><span class="icon-career-planning-icon icon-md nav-color" aria-hidden="true"></span>Career Planning</a></li>
          <li><a href="#"><span class="icon-personal-branding-icon icon-md nav-color" aria-hidden="true"></span>Personal Branding</a></li>
          <li><a href="#"><span class="icon-job-search-icon icon-md nav-color" aria-hidden="true"></span>Job Search</a></li>
          <li><a href="#"><span class="icon-close-deal-icon icon-md nav-color" aria-hidden="true"></span>Close the Deal</a></li>             
        </ul>
        <div class="sb-footer navbar hidden-sm hidden-md hidden-lg">
          <ul class="footerNav "> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </div>
      <!-- SIDEBAR MENU END -->

      
      <!-- CONTAINER WRAP START -->
      <div class="container_wrp">
        <div class="dash_wrp">
          <div class="row">
            <h1>Profile</h1>
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <h4 class="grayTitle">Personal Info <a id="edit_name"  href="#"> <i class="glyphicon glyphicon-edit right">edit</i></a> <a id="save_name" href="#"> <i class="glyphicon glyphicon-floppy-save right">Save</i></a></h4>
              <div class="right col-xs-12 col-sm-12 col-md-12 personal-info">
                <div class="row">
                  <div class="mr4 col-xs-12 col-sm-5 col-lg-5">
                    <div class="form-group has-feedback">
                      <label for="firstName">First Name</label>
                      <div class="fieldValue profile_f_pad">Darell</div>
                      <input type="text" class="form-control hiddenInput" id="firstName" name="fname" placeholder="Darell">
                    </div>
                    <div class="form-group has-feedback">
                      <label for="lastName">Last Name</label>
                      <div class="fieldValue profile_f_pad">Jackson</div>
                      <input type="text" class="form-control hiddenInput" id="lastName" name="lname" placeholder="Jackson">
                    </div>
                    <div class="form-group has-feedback">
                      <label for="emailAddress">Email Address</label><br>
                      <div class="fieldValue profile_f_pad">email@website.com</div>
                      <input type="text" class="form-control hiddenInput" placeholder="email@website.com" id="emailAddress" name="eAddress">
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-5 col-lg-5">
                    <div class="form-group has-feedback">
                      <label for="emailAddress">Phone</label>
                      <div class="fieldValue profile_f_pad">123-123-1234</div>
                      <input type="text" class="form-control hiddenInput" placeholder="123-123-1234" id="emailAddress" name="eAddress">
                    </div>
                    <label for="pLanguage">Preferred Language</label>
                    <select class="regLanguage" name="pLanguage" id="pLanguage">
                      <option value="english">English</option>
                      <option value="spanish">Spanish</option>
                      <option value="sample">Sample</option>
                    </select>
                  </div>
                </div>
              </div> 
            </div>
            <div class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left">
              <h4 class="grayTitle">My Current Role</h4>
              <div class="col-xs-12 col-sm-12 col-lg-12 pwordProfile">
                <div class="form-group has-feedback">
                  <label for="company">Company</label>
                  <input type="text" class="form-control" id="company" name="company">
                </div>
                <div class="form-group has-feedback">
                  <label for="JobTitle">Job Title</label>
                  <input type="text" class="form-control" id="jobTitle" name="jobTitle">
                </div>
                <div class="form-group has-feedback">
                  <select class="last-salary" name="lastSalary" id="lastSalary">
                      <option value="english">Salary Compared to Your Last Position</option>
                      <option value="spanish">more than</option>
                      <option value="spanish">same as</option>
                      <option value="sample">less than</option>
                    </select>
                </div>
                <div class="reg_bntWRP">
                  <input class="continue contBTN col-xs-12 col-sm-8 col-lg-8" type="button" value="Edit" style="margin:2em 0;">
                </div>
              </div>
            </div>
            <div class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right">
              <h4 class="grayTitle">Password Info</h4>
              <div class="col-xs-12 col-sm-12 col-lg-12 pwordProfile">
                <div class="form-group has-feedback">
                  <label for="password">Current Password</label>
                  <input type="text" class="form-control" id="password" name="password">
                </div>
                <div class="form-group has-feedback">
                  <label for="newpassword">New Password</label>
                  <input type="text" class="form-control" id="newPassword" name="newPassword">
                </div>
                <div class="form-group has-feedback">
                  <label for="ConfirmPassword">Confirm Password</label>
                  <input type="text" class="form-control" id="ConfirmPassword" name="ConfirmPassword">
                </div>
                <div class="reg_bntWRP">
                  <input class="continue contBTN col-xs-12 col-sm-8 col-lg-8" type="button" value="Save Changes" style="margin:2em 0;">
                </div>
              </div>
            </div>
            <div class="row">
        <div class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left">
          <h5>ASSESSMENT RESULTS</h5>
          <div class="p2">
            <ul class="bullet2">
              <li><a href="#">Determine Value And Drive</a></li>
              <li class="incomplete"><a href="#">Complete thought provoking statements</a></li>
              <li><a href="#">Analyze your strengths</a></li>
              <li><a href="#">Identify your ideal position</a></li>
              <li class="incomplete"><a href="#">Look at options</a></li>
              <li><a href="#">Resume Builder</a></li>
              <li class="incomplete"><a href="#">Prepare for a job search</a></li>
              <li class="incomplete"><a href="#">Prepare for an interview</a></li>
              <li class="incomplete"><a href="#">Evaluate an offer</a></li>
            </ul>
          </div>
        </div>
        <div class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right">
          <h5>MY DOCUMENTS</h5>
          <div class="p2">
              <ul class="bullet2">
              <li><a href="#">Chronological Resume 10/10/14</a></li>
              <li><a href="#">Functional Resume 8/16/14</a></li>
              <li><a href="#">Curriculum Vitae 6/21/14</a></li>
              <li><a href="#">Chronological Resume 6/20/14</a></li>
            </ul>
            <a class="upload"><span class="glyphicon glyphicon-upload"></span> UPLOAD DOCUMENT</a>
          </div>
        </div>
      </div>
      
    </div><!--/.row-->
   </div>
 </div>

      <footer class="dashfooter navbar hidden-xs">
      <!-- INCLUDE FOOTER IN FINAL CODE  < ?php include 'includes/footer.php' ;?> -->
        <div class="inner-footer">
          <div class="footer-logo">
            <img id="ft-logo" src="images/logo.png" alt="Right Mangement Logo" />
          </div>
          <ul class="footerNav"> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </footer> 
    </div><!-- END MAIN WRAPPER --> 

    
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   
      <script src="js/modernizr.custom.js"></script>
     
    <script>
         // SVG LOGO DETECT // 
        if (Modernizr.svg)
        {
          $("#logo").attr("src", "images/right_logo.svg");
          //console.log('changed');

          $("#ft-logo").attr("src", "images/logo.svg");
          //1console.log('changed');
        }; 

        // MOBILE SIDE MENU TOGGLE // 
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

         $("#close-menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").removeClass("toggled");
        });
        
        // HIDES MOBILE SUB MENU //
        $(".carrer_coach").click(function(e) {
            e.preventDefault();
            $("#coach-info").toggleClass("invisible");
        });

        $(".help").click(function(e) {
            e.preventDefault();
            $("#help-info").toggleClass("invisible");
        });



         $(function() {
      $("#formName").hide();
      $("#save_name").hide();
      $(".hiddenInput").hide();
       $(".hiddenTest").hide();
      });

        $("#edit_name").click(function(){
    
      $("#displayName").fadeTo( "fast" , 0) .hide();
      $("#formName").fadeTo( "fast" , 1);
      $(".fieldValue").fadeTo( "fast" , 0) .hide();
      $(".hiddenInput").fadeTo( "fast" , 1);
      $(".hiddenTest").show();
        $("#save_name").show();  
        $("#edit_name").hide();   
        });

         $("#save_name").click(function(){
    
      
      $("#formName").fadeTo( "fast" , 0) .hide();  
      $("#displayName").fadeTo( "fast" , 1) ;
      $(".hiddenInput").fadeTo( "fast" , 0).hide();
      $(".fieldValue").fadeTo( "fast" , 1);
      $(".hiddenTest").hide();
      $("#save_name").hide();  
        $("#edit_name").show();     
        });


     </script>
    

  </body>
  </html>
