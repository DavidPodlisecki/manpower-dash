<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Manpower RightEverywhere </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/icon-style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="resourcesPage" class="dashboard">
    <!-- START HEADER -->
    <div class="header_wrp navbar navbar-default"  role="navigation">
      <a  href="#menu-toggle" id="menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </a>
      <div class="dash_logo">
        <img id="logo" src="images/logo.png" alt="Right Mangement Logo" />
      </div>
      
      <ul class="header_nav hidden-xs">
        <li><a href="#" >Resources</a></li>
        <li><a href="#">Log Out</a></li>
      </ul>
      <a href="#" class="hidden-xs" id="messages">
        <span>3</span>
      </a>
    </div>
    <!-- END HEADER -->

    <!-- START WRAPPER -->
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
       <!-- SIDEBAR MENU START -->
      <div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
        <a href="#clost-menu-toggle" class="close-toggle" id="close-menu-toggle">X</a>
        <ul class="nav top-nav hidden-sm hidden-md hidden-lg">
          <li><a class="noICO" href="#">NOTIFICATIONS</a></li>
          <li><a class="noICO" href="#">RESOURCES</a></li>
          <li><a class="noICO" href="#">LOG OUT</a>
        </ul>
        <ul class="nav orange-icon" id="orange_icon">
          <li><div class="navTitle">MAIN</div></li>
          <li><a href="#"><span class="icon-dashboard-icon icon-md nav-color" aria-hidden="true"></span>Dashboard</a></li>
          <li><a href="#"><span class="icon-profile-icon icon-md nav-color" aria-hidden="true"></span>Profile</a></li>
          <li><a class="carrer_coach" href="#"><span class="icon-career-coach-icon icon-md nav-color" aria-hidden="true"></span>Career Coach</a>
            <ul>
              <li id="coach-info">
                <h4>Han Solo</h4>
                <div class="phone">123-123-1234</div> 
                <div class="email">hansolo@galaxy.com</div>
              </li>
            </ul>
          </li>
          <li><a href="#"><span class="icon-events-icon icon-md nav-color" aria-hidden="true"></span>Events &amp; Webinars</a></li>
          <li><a href="#"><span class="icon-favorite-icon icon-md nav-color" aria-hidden="true"></span>Favorites</a></li> 
          <li><a class="help" href="#"><span class="icon-help-icon icon-md nav-color" aria-hidden="true"></span>Help</a>
            <ul>
              <li id="help-info" class="help-desk">
                <h4>Help Desk</h4>
                <div class="p2">
                  <p>For assistance with RightEverywhere technical questions and quick job search coaching questions, please call <strong>800.668.8556</strong> </p>
                  <p>8AM- 8PM Eastern (Mon - Fri) <br>9AM- 5PM Eastern (Sat - Sun)</p>
                </div>
              </li>
            </ul>
          </li>
          <li><div class="navTitle">PROGRESS</div></li>
          <li><a href="#"><span class="icon-discovery-icon icon-md nav-color" aria-hidden="true"></span>Self Discovery</a></li>
          <li><a href="#"><span class="icon-career-planning-icon icon-md nav-color" aria-hidden="true"></span>Career Planning</a></li>
          <li><a href="#"><span class="icon-personal-branding-icon icon-md nav-color" aria-hidden="true"></span>Personal Branding</a></li>
          <li><a href="#"><span class="icon-job-search-icon icon-md nav-color" aria-hidden="true"></span>Job Search</a></li>
          <li><a href="#"><span class="icon-close-deal-icon icon-md nav-color" aria-hidden="true"></span>Close the Deal</a></li>             
        </ul>
        <div class="sb-footer navbar hidden-sm hidden-md hidden-lg">
          <ul class="footerNav "> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </div>
      <!-- SIDEBAR MENU END -->
      
      <!-- START PAGE CONTAINER -->
      <div class="container_wrp" id="container_wrp">
        <div class="dash_wrp">
          <div class="row">
            <h1><span class="icon-assessments-icon blue" aria-hidden="true"></span>Job Search</h1>  
            
            
            <!-- START CONTAINER --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <div class="p2">
                <h4>Getting Started in Search</h4>
                  Vestibulum id ligula porta felis euismod semper. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna.
                  <div class="row" >
                    <div class="top-border"></div>
                    <div class="col-xs-12 col-sm-15 col-md-15 col-lg-15 blue icon-nav-wrp">
                    <a href="#"><span class="icon-assessments-icon icon-lg left" aria-hidden="true"></span><span class="icon-text">Assesments &amp; Tasks</span></a>
                    </div> 
                    <div class="col-xs-12 col-sm-15 col-md-15 col-lg-15 blue icon-nav-wrp" >
                    <a href="#"><span class="icon-job-openings-icon icon-lg left" aria-hidden="true"></span><span class="icon-text">Job Openings</span></a>
                    </div> 
                    <div class="col-xs-12 col-sm-15 col-md-15 col-lg-15 blue icon-nav-wrp" >
                    <a href="#"><span class="icon-webinars-icon icon-lg left" aria-hidden="true"></span><span class="icon-text">Events &amp; Webinars</span></a>
                    </div> 
                    <div class="col-xs-12 col-sm-15 col-md-15 col-lg-15 blue icon-nav-wrp" >
                    <a href="#"><span class="icon-resources-icon icon-lg left" aria-hidden="true"></span><span class="icon-text">Resources</span></a>
                    </div> 
                    <div class="col-xs-12 col-sm-15 col-md-15 col-lg-15 blue icon-nav-wrp" >
                    <a href="#"><span class="icon-tools-icon icon-lg left" aria-hidden="true"></span><span class="icon-text">Tools</span></a>
                    </div>  
                </div>
              </div>
            </div><!-- END CONTAINER --> 

            <div class="dash_container">
              <div class="container_heading_sm">JOB SEARCH: <span>Assesments &amp; Tasks </span><div class="icon-assessments-icon icon-lg right blue" aria-hidden="true"></div></div>
              <div class="p2">
                <div class="stepTitle black">Create a Job Search Agent on Right Jobs</div>
                <p>Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. Eu autem invidunt neglegentur pro, iusto audire platonem in nec.</p>
                <div class="timeToTake blue_txt">TIME TO TAKE: <span>Approximately 20 minutes</span></div>
                <a class="button orange" href="#">Get Started</a>
                <a href="#" class="link ">SKIP: <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
             
               <div class="mid-border"></div>
              
                <div class="stepTitle black">Prepare for an Interview</div>
                <p>Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. Eu autem invidunt neglegentur pro, iusto audire platonem in nec.</p>
                <div class="timeToTake blue_txt">TIME TO TAKE: <span>Approximately 20 minutes</span></div>
                <a class="button blue" href="#">In Progress</a>
                <a href="#" class="link ">SKIP: <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
              
              <div class="mid-border"></div>

              <div class="stepTitle gray_txt ">Birkman Assessment</div>
                <p class="gray_txt">Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. Eu autem invidunt neglegentur pro, iusto audire platonem in nec.</p>
                <div class="timeToTake blue_txt">This resource may be important to you because this, this and this.</div>
                <a class="button orange" href="#">Purchase</a>
                
              </div>

              <div class="p2">
                <div class="right w100 p2 lt-gray-bx rd5 mb1-0">
                      <div class="left"><span class="fs1-5 lite">Close The Deal</span> 
                      <div class="timeToTake green_txt">STATUS: <span>Completed</span> </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-7 right">
                      <a class="left ls1 mt-7 mb0" href="#"><span class="icon-reset-icon icon-md left" aria-hidden="true"></span><span class="lh2-2 ls2" id="reset" >RETAKE</span></a><a class="orange_INV_link button right mt1 mb0" href="resume_builder_step1.php">REVIEW RESULTS</a>
                      </div>
                    </div>

                    <div class="right w100 p2 lt-gray-bx rd5 mb1-0">
                      <div class="left"><span class="fs1-5 lite">Practice Interviewing</span>
                      <div class="timeToTake green_txt">STATUS: <span>Completed</span> </div> </div>
                     <div class="col-xs-12 col-sm-12 col-md-7 right">
                      <a class="left ls1 mt-7 mb0" href="#"><span class="icon-reset-icon icon-md left" aria-hidden="true"></span><span class="lh2-2 ls2" id="reset" >RETAKE</span></a><a class="orange_INV_link button right mt1 mb0" href="resume_builder_step1.php">REVIEW RESULTS</a>
                      </div>
                    </div>

                    
              </div>
            </div>

            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <div class="container_heading_sm">JOB SEARCH: <span>Job Openings on Right Jobs</span><div class="icon-job-openings-icon icon-lg right blue" aria-hidden="true"></div></div>
              <div class="gray-heading">
                <div class="col-md-8">JOBS</div>
                <div class="col-md-4 hidden-xs hidden-sm"> LOCATION</div>
              </div>
              <ul id="job-search-list">
                <li class="job-post">
                  <a href="#">
                  <div class="col-md-8"><span class="job-title">Position Title</span><span class="comp-title">Company name</span></div>
                  <div class="col-md-4 location">Milwaukee, Wi</div>
                  </a>
                </li> 
                <li class="job-post">
                  <a href="#">
                  <div class="col-md-8"><span class="job-title">Position Title</span><span class="comp-title">Company name</span></div>
                  <div class="col-md-4 location">Milwaukee, Wi</div>
                  </a>
                </li>
                <li class="job-post">
                  <a href="#">
                  <div class="col-md-8"><span class="job-title">Position Title</span><span class="comp-title">Company name</span></div>
                  <div class="col-md-4 location">Milwaukee, Wi</div>
                  </a>
                </li>
                <li class="job-post">
                  <a href="#">
                  <div class="col-md-8"><span class="job-title">Position Title</span><span class="comp-title">Company name</span></div>
                  <div class="col-md-4 location">Milwaukee, Wi</div>
                  </a>
                </li>
              </ul>
            </div>


            <!-- START JOB SEARCH -->
            <div class="dash_container col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="container_heading_sm">JOB SEARCH: <span>Events &amp; Webinars</span><div class="icon-webinars-icon icon-lg right blue" aria-hidden="true"></div></div>
            </div>
            <div class="row">
              <div class="dash_container js-event-wrp-xs js-event-wrp-sm js-event-wrp-md js-event-wrp-lg">
                <h5 class="event-title"><span>Advanced Linkedin</span></h5>
                <p class="event-info">Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum </p>
                <div class="duration">1hr 10 min</div>
              </div>
              <div class="dash_container js-event-wrp-xs js-event-wrp-sm js-event-wrp-md js-event-wrp-lg">
                <h5 class="event-title"><span>Nonprofit Job Search</span></h5>
                <p class="event-info">Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum</p>
                <div class="duration">53 min</div>
              </div>
              <div class="dash_container js-event-wrp-xs js-event-wrp-sm js-event-wrp-md js-event-wrp-lg">
                <h5 class="event-title"><span>Creating My Personal Brand</span></h5>
                <p class="event-info">Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum </p>
                <div class="duration">15 min</div>
              </div>
              <div class="dash_container js-event-wrp-xs js-event-wrp-sm js-event-wrp-md js-event-wrp-lg">
                <h5 class="event-title"><span>Advanced Linkedin</span></h5>
                <p class="event-info">Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum </p>
                <div class="duration">1hr 10 min</div>
              </div>
              <div class="dash_container js-event-wrp-xs js-event-wrp-sm js-event-wrp-md js-event-wrp-lg">
                <h5 class="event-title"><span>Nonprofit Job Search</span></h5>
                <p class="event-info">Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum</p>
                <div class="duration">53 min</div>
              </div>
              <div class="dash_container js-event-wrp-xs js-event-wrp-sm js-event-wrp-md js-event-wrp-lg">
                <h5 class="event-title"><span>Creating My Personal Brand</span></h5>
                <p class="event-info">Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum </p>
                <div class="duration">15 min</div>
              </div>
            </div>
            
              <div class="moreBtn"><a href="#" class="orange button">SEE MORE EVENTS &amp; WEBINARS</a></div>
           
            <!-- END JOB SERCH -->
 
             <div class="row">
        <div class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left moreBtnSM-container">
         <div class="container_heading_sm">JOB SEARCH: <span>Resources</span><div class="icon-resources-icon icon-md right blue" aria-hidden="true"></div></div>
          <div class="p2">
            <div class="sm-job-research">
              <h6>Plan Resource</h6>
              This phase of Life Options is all about discovery - discovering what will form the foundation of your plan for achieving the kind of life that you would consider most fulfilling.
            </div>
           
            <div class="sm-job-research">
              <h6>Plan Resource 2</h6>
              This phase of Life Options is all about discovery - discovering what will form the foundation of your plan for achieving the kind of life that you would consider most fulfilling.
            </div>
           <div class="moreBtnSm"><a href="#" class="orange button">MORE RESOURCES</a></div>
          </div>
        </div>
        <div class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right">
          <div class="container_heading_sm">JOB SEARCH: <span>Tools</span><div class="icon-tools-icon icon-md right blue" aria-hidden="true"></div></div>
          <div class="p2">
               <div class="sm-job-research">
              <h6>Plan Resource</h6>
              This phase of Life Options is all about discovery - discovering what will form the foundation of your plan for achieving the kind of life that you would consider most fulfilling.
            </div>
           
            <div class="sm-job-research">
              <h6>Plan Resource 2</h6>
              This phase of Life Options is all about discovery - discovering what will form the foundation of your plan for achieving the kind of life that you would consider most fulfilling.
            </div>
          </div>
        </div>
      </div>
        
          </div><!-- END ROW -->
        </div><!-- END DASH WRAP -->
      </div><!-- END PAGE CONTAINER -->     
      <!-- BACK TO TOP BUTTON -->
       <a href="#" class="back-to-top">Back to Top</a> 
      <!-- END BACK TO TOP -->
      <footer class="dashfooter navbar hidden-xs">
      <!-- INCLUDE FOOTER IN FINAL CODE  < ?php include 'includes/footer.php' ;?> -->
        <div class="inner-footer">
          <div class="footer-logo">
            <img id="ft-logo" src="images/logo.png" alt="Right Mangement Logo" />
          </div>
          <ul class="footerNav"> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </footer> 
    </div><!-- END MAIN WRAPPER --> 
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   

    
      <script src="js/modernizr.custom.js"></script>
     
    <script>
        // SVG LOGO DETECT // 
        if (Modernizr.svg)
        {
          $("#logo").attr("src", "images/right_logo.svg");
          console.log('changed');

          $("#ft-logo").attr("src", "images/logo.svg");
          console.log('changed');
        }; 

        // MOBILE SIDE MENU TOGGLE // 
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

         $("#close-menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").removeClass("toggled");
        });
        
        // HIDES MOBILE SUB MENU //
        $(".carrer_coach").click(function(e) {
            e.preventDefault();
            $("#coach-info").toggleClass("invisible");
        });

        $(".help").click(function(e) {
            e.preventDefault();
            $("#help-info").toggleClass("invisible");
        });

        // SCRIPT FOR BACK TO TOP
        $(document).ready(function() {
          var offset = 220;
          var duration = 500;
          $(window).scroll(function() {
              if ($(this).scrollTop() > offset) {
                  $('.back-to-top').fadeIn(duration);
              } else {
                  $('.back-to-top').fadeOut(duration);
              }
          });
          
          $('.back-to-top').click(function(event) {
              event.preventDefault();
              $('html, body').animate({scrollTop: 0}, duration);
              return false;
          })
      });

     // RESIZE FIX //
        $(function(){
        var windowH = $(window).height();
        var wrapperH = $('.dash_wrp').height()+177;
        var navHeignt = $('#orange_icon').height()+77;
        
        if(windowH > wrapperH) {                            
            $('#wrapper').css({'height':($(window).height()-85)+'px'});
            $('.dashboard').css('margin-bottom', '-86px');
        } 
        else if(navHeignt < windowH && wrapperH < navHeignt) {                            
            $('#wrapper').css('height', (navHeignt)+'px');
            $('.dashboard').css('margin-bottom', '-86px');
        } 

        function windowResizer(){
            var windowH = $(window).height();
            var wrapperH = $('.dash_wrp').height()+177;
            var differenceH = windowH - wrapperH;
            var newH = wrapperH + differenceH;
            var truecontentH = $('.dash_wrp').height()+160;
            var navHeignt = $('#orange_icon').height()+77;
            if(windowH < navHeignt && wrapperH > navHeignt) {
                $('#wrapper').css('height', (navHeignt)+'px');
                 $('.dashboard').css('margin-bottom', '-86px');
                console.log("windowResizer Worked 1")
            }
            else if(windowH > truecontentH) {
                $('#wrapper').css('height', (newH)-86+'px');
                 $('.dashboard').css('margin-bottom', '-86px');
                 console.log("windowResizer Worked 2")
        } }
                                                                                       
        $(window).resize(function(){
             windowResizer()

          })
        // END RESIZE FIX    //
        
     </script>
    

  </body>
</html>
