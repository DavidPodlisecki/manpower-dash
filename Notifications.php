<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Manpower RightEverywhere </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/icon-style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="notificationsPage" class="dashboard">
    <!-- START HEADER -->
    <div class="header_wrp navbar navbar-default"  role="navigation">
      <a  href="#menu-toggle" id="menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </a>
      <div class="dash_logo">
        <img id="logo" src="images/right_logo.png" alt="Right Mangement Logo" />
      </div>
      
      <ul class="header_nav hidden-xs">
        <li><a href="#" >Resources</a></li>
        <li><a href="#">Log Out</a></li>
      </ul>
      <a href="#" class="hidden-xs" id="messages">
        <span>3</span>
      </a>
    </div>
    <!-- END HEADER -->

    <!-- START WRAPPER -->
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
      <!-- SIDEBAR MENU START -->
      <div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
        <a href="#clost-menu-toggle" class="close-toggle" id="close-menu-toggle">X</a>
        <ul class="nav top-nav hidden-sm hidden-md hidden-lg">
          <li><a class="noICO" href="#">NOTIFICATIONS</a></li>
          <li><a class="noICO" href="#">RESOURCES</a></li>
          <li><a class="noICO" href="#">LOG OUT</a>
        </ul>
        <ul class="nav orange-icon" id="orange_icon">
          <li><div class="navTitle">MAIN</div></li>
          <li><a href="#"><span class="icon-dashboard-icon icon-md nav-color" aria-hidden="true"></span>Dashboard</a></li>
          <li><a href="#"><span class="icon-profile-icon icon-md nav-color" aria-hidden="true"></span>Profile</a></li>
          <li><a class="carrer_coach" href="#"><span class="icon-career-coach-icon icon-md nav-color" aria-hidden="true"></span>Career Coach</a>
            <ul>
              <li id="coach-info">
                <h4>Han Solo</h4>
                <div class="phone">123-123-1234</div> 
                <div class="email">hansolo@galaxy.com</div>
              </li>
            </ul>
          </li>
          <li><a href="#"><span class="icon-events-icon icon-md nav-color" aria-hidden="true"></span>Events &amp; Webinars</a></li>
          <li><a href="#"><span class="icon-favorite-icon icon-md nav-color" aria-hidden="true"></span>Favorites</a></li> 
          <li><a class="help" href="#"><span class="icon-help-icon icon-md nav-color" aria-hidden="true"></span>Help</a>
            <ul>
              <li id="help-info" class="help-desk">
                <h4>Help Desk</h4>
                <div class="p2">
                  <p>For assistance with RightEverywhere technical questions and quick job search coaching questions, please call <strong>800.668.8556</strong> </p>
                  <p>8AM- 8PM Eastern (Mon - Fri) <br>9AM- 5PM Eastern (Sat - Sun)</p>
                </div>
              </li>
            </ul>
          </li>
          <li><div class="navTitle">PROGRESS</div></li>
          <li><a href="#"><span class="icon-discovery-icon icon-md nav-color" aria-hidden="true"></span>Self Discovery</a></li>
          <li><a href="#"><span class="icon-career-planning-icon icon-md nav-color" aria-hidden="true"></span>Career Planning</a></li>
          <li><a href="#"><span class="icon-personal-branding-icon icon-md nav-color" aria-hidden="true"></span>Personal Branding</a></li>
          <li><a href="#"><span class="icon-job-search-icon icon-md nav-color" aria-hidden="true"></span>Job Search</a></li>
          <li><a href="#"><span class="icon-close-deal-icon icon-md nav-color" aria-hidden="true"></span>Close the Deal</a></li>             
        </ul>
        <div class="sb-footer navbar hidden-sm hidden-md hidden-lg">
          <ul class="footerNav "> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </div>
      <!-- SIDEBAR MENU END -->
      
      <!-- START PAGE CONTAINER -->
      <div class="container_wrp" id="container_wrp">
        <div class="dash_wrp">
          <div class="row">
            <h1>Notifications</h1>  
            
            <div class="dash_container col-xs-12 col-sm-12 col-md-12" id="notification1">
              <a class="close_notification" id="close1">x</a>
              <div class="p2 red notification">
                
                  Righteverywhere.com will be down for maintenance from 8am on 12/12/14 until 2pm on 12/13/14. Please plan accordingly.
                  
              </div>
            </div>
          
            <div class="dash_container col-xs-12 col-sm-12 col-md-12" id="notification2">
              <a class="close_notification" id="close2">x</a>
              <div class="p2 notification">
                
                Your righteverywhere.com program will end on 3/29/15.
                  
              </div>
            </div>

            <div class="dash_container col-xs-12 col-sm-12 col-md-12" id="notification3">
              <a class="close_notification" id="close3">x</a>
              <div class="p2 notification">
                
                 A new skills soft program is available for you. <a href="#">Check it out here</a>

              </div>
            </div>
            <!-- 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12" id="notification4">
              <a class="close_notification" id="close4">x</a>
              <div class="p2 notification">
                
                  New resources have been added to your program. <a href="#">Check them out here</a>

              </div>
            </div>

            <div class="dash_container col-xs-12 col-sm-12 col-md-12" id="notification5">
              <a class="close_notification" id="close5">x</a>
              <div class="p2 notification">
                
                  A new event, EVENT NAME has been scheduled at your office.<a href="#">Register here</a>

              </div>
            </div> 
             -->
          </div><!-- END ROW -->
        </div><!-- END DASH WRAP -->
      </div><!-- END PAGE CONTAINER -->     
        
      <footer class="dashfooter navbar hidden-xs">
      <!-- INCLUDE FOOTER IN FINAL CODE  < ?php include 'includes/footer.php' ;?> -->
        <div class="inner-footer">
          <div class="footer-logo">
            <img id="ft-logo" src="images/logo.png" alt="Right Mangement Logo" />
          </div>
          <ul class="footerNav"> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </footer> 
    </div><!-- END MAIN WRAPPER --> 
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   

      <script src="js/modernizr.custom.js"></script>
     
    <script>
    // RESIZE FIX //
        $(function(){
        var windowH = $(window).height();
        var wrapperH = $('.dash_wrp').height()+177;
        var navHeignt = $('#orange_icon').height()+77;
        if(windowH < navHeignt && wrapperH < navHeignt) {                            
            $('#wrapper').css('height', (navHeignt)+'px');
            $('.dashboard').css('margin-bottom', '-86px');
        } 
        else if(windowH > wrapperH) {                            
            $('#wrapper').css({'height':($(window).height()-85)+'px'});
            $('.dashboard').css('margin-bottom', '-86px');
        } 
        

        function windowResizer(){
            var windowH = $(window).height();
            var wrapperH = $('.dash_wrp').height()+177;
            var differenceH = windowH - wrapperH;
            var newH = wrapperH + differenceH;
            var truecontentH = $('.dash_wrp').height()+177;
            var navHeignt = $('#orange_icon').height()+77;
            if(windowH < navHeignt && wrapperH < navHeignt) {
                $('#wrapper').css('height', (navHeignt)+'px');
                 $('.dashboard').css('margin-bottom', '-86px');
                console.log("windowResizer Worked 1")
            }
            else if(windowH > truecontentH) {
                $('#wrapper').css('height', (newH)-86+'px');
                 $('.dashboard').css('margin-bottom', '-86px');
                 console.log("windowResizer Worked 2")
        } }
                                                                                       
        $(window).resize(function(){
             windowResizer()

          })
        // END RESIZE FIX    //


        // SVG LOGO DETECT // 
        if (Modernizr.svg)
        {
          $("#logo").attr("src", "images/right_logo.svg");
          console.log('changed');

          $("#ft-logo").attr("src", "images/logo.svg");
          console.log('changed');
        }; 

        $("#close1").click(function(){
          $("#notification1").hide('slow');
          windowResizer();
        });
        $("#close2").click(function(){
          $("#notification2").hide('slow');
          windowResizer();
        });
        $("#close3").click(function(){
          $("#notification3").hide('slow');
          windowResizer();
        });
        $("#close4").click(function(){
          $("#notification4").hide('slow');
          windowResizer();
        });
        $("#close5").click(function(){
          $("#notification5").hide('slow');
          windowResizer()
        });

        // MOBILE SIDE MENU TOGGLE // 
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

         $("#close-menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").removeClass("toggled");
        });
        
        // HIDES MOBILE SUB MENU //
        $(".carrer_coach").click(function(e) {
            e.preventDefault();
            $("#coach-info").toggleClass("invisible");
        });

        $(".help").click(function(e) {
            e.preventDefault();
            $("#help-info").toggleClass("invisible");
        });

        
             
    });
        
     </script>
    

  </body>
</html>
