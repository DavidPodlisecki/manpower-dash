<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Manpower RightEverywhere </title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/icon-style.css" rel="stylesheet">
    <link type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet" />  
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="strengthsPage" class="dashboard">
    <!-- START HEADER -->
    <div class="header_wrp navbar navbar-default"  role="navigation">
      <a  href="#menu-toggle" id="menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </a>
      <div class="dash_logo">
        <img id="logo" src="../right_images/logo.png" alt="Right Mangement Logo" />
      </div>
      
      <ul class="header_nav hidden-xs">
        <li><a href="#" >Resources</a></li>
        <li><a href="#">Log Out</a></li>
      </ul>
      <a href="#" class="hidden-xs" id="messages">
        <span>3</span>
      </a>
    </div>
    <!-- END HEADER -->
 
    <!-- START WRAPPER -->
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
      <!-- SIDEBAR MENU START -->
      <div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
        <a href="#clost-menu-toggle" class="close-toggle" id="close-menu-toggle">X</a>
        <ul class="nav top-nav hidden-sm hidden-md hidden-lg">
          <li><a class="noICO" href="#">NOTIFICATIONS</a></li>
          <li><a class="noICO" href="#">RESOURCES</a></li>
          <li><a class="noICO" href="#">LOG OUT</a>
        </ul>
        <ul class="nav orange-icon" id="orange_icon">
          <li><div class="navTitle">MAIN</div></li>
          <li><a class="active" href="#"><span class="icon-dashboard-icon icon-md nav-color" aria-hidden="true"></span>Dashboard</a></li>
          <li><a href="#"><span class="icon-profile-icon icon-md nav-color" aria-hidden="true"></span>Profile</a></li>
          <li><a class="carrer_coach" href="#"><span class="icon-career-coach-icon icon-md nav-color" aria-hidden="true"></span>Career Coach</a>
            <ul>
              <li id="coach-info">
                <h4>Han Solo</h4>
                <div class="phone">123-123-1234</div> 
                <div class="email">hansolo@galaxy.com</div>
              </li>
            </ul>
          </li>
          <li><a href="#"><span class="icon-events-icon icon-md nav-color" aria-hidden="true"></span>Events &amp; Webinars</a></li>
          <li><a href="#"><span class="icon-favorite-icon icon-md nav-color" aria-hidden="true"></span>Favorites</a></li> 
          <li><a class="help" href="#"><span class="icon-help-icon icon-md nav-color" aria-hidden="true"></span>Help</a>
            <ul>
              <li id="help-info" class="help-desk">
                <h4>Help Desk</h4>
                <div class="p2">
                  <p>For assistance with RightEverywhere technical questions and quick job search coaching questions, please call <strong>800.668.8556</strong> </p>
                  <p>8AM- 8PM Eastern (Mon - Fri) <br>9AM- 5PM Eastern (Sat - Sun)</p>
                </div>
              </li>
            </ul>
          </li>
          <li><div class="navTitle">PROGRESS</div></li>
          <li><a href="#"><span class="icon-discovery-icon icon-md nav-color" aria-hidden="true"></span>Self Discovery</a></li>
          <li><a href="#"><span class="icon-career-planning-icon icon-md nav-color" aria-hidden="true"></span>Career Planning</a></li>
          <li><a href="#"><span class="icon-personal-branding-icon icon-md nav-color" aria-hidden="true"></span>Personal Branding</a></li>
          <li><a href="#"><span class="icon-job-search-icon icon-md nav-color" aria-hidden="true"></span>Job Search</a></li>
          <li><a href="#"><span class="icon-close-deal-icon icon-md nav-color" aria-hidden="true"></span>Close the Deal</a></li>             
        </ul>
        <div class="sb-footer navbar hidden-sm hidden-md hidden-lg">
          <ul class="footerNav "> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </div>
      <!-- SIDEBAR MENU END -->
      
      <!-- START PAGE CONTAINER -->
      <div class="container_wrp" id="container_wrp">
        <div class="dash_wrp">
          <div class="row">
            <a href="#" class="left backBTN"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Back To Self Discovery</a>
            
            <!-- START RESOURCE --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <div class="p2">
                <h4 class="resource blue_txt">Reviewing Accomplishments</h4>
                <div class="right col-xs-12 col-sm-12 col-md-12">
                  <div class="row">
                    <h6 class="resource_list_title">Instructions</h6>
                    <p>
                      <ul>
                        <li>Follow the prompts to choose your format and input your work history. </li>
                        <li>Refer to resume examples as necessary. These will help you see the full picture of what you are creating.</li>
                        <li>You do not need to fill in every section. If something is not applicable to you, skip it and move to the next. </li>
                      </ul>
                    </p>
                  
                  </div> 
                  
                  <a class="green button right mb2" href="#">EXIT</a><a class="blue button right mb2" href="#">SAVE</a> 
                   
                </div>
              </div>
            </div><!-- END RESOURCE --> 

            <h4 class="info_header col-xs-12 col-sm-12 col-md-12">Key Accomlishments</h4>


            <div role="tabpanel col-xs-12 col-sm-12 col-md-12 right">

              <!-- Nav tabs -->
              <ul class="nav nav-tabs responsive" role="tablist" id="test-this">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="ls1">CHALLENGE</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="ls1">ACTION</a></li>
                <li role="presentation"><a href="#result" aria-controls="messages" role="tab" data-toggle="tab" class="ls1">RESULT</a></li>
                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab" class="ls1">SKILLS</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content responsive">
                <div role="tabpanel" class="tab-pane active" id="home">
                  <form id="summary" class="square_input">
                    <p><strong>Accomplishment 1</strong></p>
                                        <div class="form-group has-feedback">
                        <label for="summary" class="hiddenLabel">Summary</label>
                       <textarea name="summary" id="summary" class="w100" rows="10"> </textarea>
                      </div>
                  </form>
                  <div class="m2_5">
                  <a type="button" class="orange button mb2 right">Next</a> 
                  <a type="button" class="orange_INV_link button mb2 right" data-dismiss="modal">Previous</a> 
                  </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="profile">
Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam quis risus eget urna mollis ornare vel eu leo. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.                
                 <div class="m2_5">
                  <a type="button" class="orange button mb2 right">Next</a> 
                  <a type="button" class="orange_INV_link button mb2 right" data-dismiss="modal">Previous</a> 
                  </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="result">
nothing yet Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam quis risus eget urna mollis ornare vel eu leo. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.                
                 <div class="m2_5">
                  <a type="button" class="orange button mb2 right">Next</a> 
                  <a type="button" class="orange_INV_link button mb2 right" data-dismiss="modal">Previous</a> 
                  </div>
                </div>

               
                <div role="tabpanel" class="tab-pane" id="settings">
                   <p class="w100"><strong>Accomplishment 1</strong></p>
                   <div class="slider-value hidden-xs"><span class="allot">USED A LOT</span> <span class="sometimes">SOMETIMES</span> <span class="not_used">NOT USED</span></div> 
                    <div class="SliderControl">
                        <label for="s1" class="col-xs-12 col-sm-8 col-lg-5">Communications</label>
                        <input type="text" class="SliderText" readonly="readonly" id="s1" name="s1"/>
                        <div class="RatingSlider" id="s1Slider"></div> 
                    </div>

                    <div class="SliderControl">
                        <label for="s2" class="col-xs-12 col-sm-8 col-lg-5">Leadership / Management</label>
                        <input type="text" class="SliderText" readonly="readonly" id="s2" name="s2"/>
                        <div class="RatingSlider" id="s2Slider"></div> 
                    </div> 

                    <div class="SliderControl">
                        <label for="s3" class="col-xs-12 col-sm-8 col-lg-5">Sales / Marketing</label>
                        <input type="text" class="SliderText" readonly="readonly" id="s3" name="s3"/>
                        <div class="RatingSlider" id="s3slider"></div> 
                    </div> 

                    <div class="SliderControl">
                        <label for="s4" class="col-xs-12 col-sm-8 col-lg-5">People - Development</label>
                        <input type="text" class="SliderText" readonly="readonly" id="s4" name="s4"/>
                        <div class="RatingSlider" id="s4Slider"></div> 
                    </div> 

                    <div class="SliderControl">
                        <label for="s5" class="col-xs-12 col-sm-8 col-lg-5">Numerical</label>
                        <input type="text" class="SliderText" readonly="readonly" id="s5" name="s5"/>
                        <div class="RatingSlider" id="s5Slider"></div> 
                    </div> 
                     <div class="m2_5">
                        <a class="lt-gray left modal1 col-xs-12" href="#editModal1" role="buton" data-toggle="modal">+ I used a skill not on this list</a>
                        <a type="button" class="orange button mb2 right">Next</a> 
                        <a type="button" class="orange_INV_link button mb2 right" data-dismiss="modal">Previous</a> 
                  </div>
                   <!-- Modal HTML -->
                    <div id="editModal1" class="modal fade ">
                        <div class="modal-dialog" style="width:450px;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h6 class="modal-title">Select the skills you would like to add to your list</h6>
                                </div>
                                <div class="modal-body">
                                  <ul class="add_skill_list">
                                    <li><input type="checkbox" name="skill_list" value="communication"><label for="communication">COMMUNICATION</label></li>
                                    <li><input type="checkbox" name="skill_list" value="problem_solving"><label for="problem_solving">PROBLEM SOLVING</label></li>
                                    <li><input type="checkbox" name="skill_list" value="project_management"><label for="project_management">PROJECT MANGEMENT</label></li>
                                    <li><input type="checkbox" name="skill_list" value="leadership"><label for="leadership">LEADERSHIP</label></li>
                                    <li><input type="checkbox" name="skill_list" value="people_mangement"><label for="people_mangement">PEOPLE - MANAGEMENT</label></li>
                                    <li><input type="checkbox" name="skill_list" value="self_mangement"><label for="self_mangement">SELF MANAGEMENT</label></li>
                                    <li><input type="checkbox" name="skill_list" value="sales_marketing"><label for="sales_marketing">SALES / MARKETING</label></li>
                                    <li><input type="checkbox" name="skill_list" value="administrations_operations"><label for="administrations_operations">ADMINISTRAIONS / OPERATIONS</label></li>
                                    <li><input type="checkbox" name="skill_list" value="technology"><label for="technology">TECHNOLOGY</label></li>
                                    <li><input type="checkbox" name="skill_list" value="people_development"><label for="people_development">PEOPLE - DEVELOPMENT</label></li>
                                    <li><input type="checkbox" name="skill_list" value="creative_conceptual"><label for="creative_conceptual">CREATIVE / CONCEPTUAL</label></li>
                                    <li><input type="checkbox" name="skill_list" value="supporting_customer_service"><label for="supporting_customer_service">SUPPORTING / CUSTOMER SERVICE</label></li>
                                    <li><input type="checkbox" name="skill_list" value="numerical"><label for="numerical">NUMERICAL</label></li>
                                    <li><input type="checkbox" name="skill_list" value="manual_process"><label for="manual_process">MANUAL / PROCESS</label></li>
                                  </ul>
                                </div>
                                <div class="modal-footer center">
                                    <button type="button" class="orange_INV button mr1" data-dismiss="modal">Close</button>
                                    <button type="button" class="orange button ">ADD</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end modal --> 
                </div>
              </div>

            </div>
            <!-- END TABS -->

          </div><!-- END ROW -->
        </div><!-- END DASH WRAP -->
      </div><!-- END PAGE CONTAINER -->     
        
      <footer class="dashfooter navbar hidden-xs">
      <!-- INCLUDE FOOTER IN FINAL CODE  < ?php include 'includes/footer.php' ;?> -->
        <div class="inner-footer">
          <div class="footer-logo">
            <img id="ft-logo" src="../images/logo.png" alt="Right Mangement Logo" />
          </div>
          <ul class="footerNav"> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </footer> 
    </div><!-- END MAIN WRAPPER --> 
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="../js/jquery.ui.touch-punch.min.js"></script>
    <script src="../js/responsive-tabs.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   

      <script src="../js/modernizr.custom.js"></script>
     
    <script type="text/javascript">
        // SVG LOGO DETECT // 
        if (Modernizr.svg)
        {
          $("#logo").attr("src", "../images/right_logo.svg");
          console.log('changed');

          $("#ft-logo").attr("src", "../images/logo.svg");
          console.log('changed');
        }; 
        

       // SLIDE CONTOLES //
       
        $('.RatingSlider').each(function(idx, elm) {
              var name = elm.id.replace('Slider', '');
          $('#' + elm.id).slider({
                  value: 2,
                  min: 0,
                  max: 4,
                  step: 1,
                  slide: function(event, ui) {
                      $('#' + name).val(ui.value);
                  }
              });
        });
  
        (function($) {
      fakewaffle.responsiveTabs(['xs', 'sm']);
  })(jQuery);

        // MOBILE SIDE MENU TOGGLE // 
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

         $("#close-menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").removeClass("toggled");
        });
        
        // HIDES MOBILE SUB MENU //
        $(".carrer_coach").click(function(e) {
            e.preventDefault();
            $("#coach-info").toggleClass("invisible");
        });

        $(".help").click(function(e) {
            e.preventDefault();
            $("#help-info").toggleClass("invisible");
        });

        
     </script>
    

  </body>
</html>
