<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Manpower RightEverywhere</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/icon-style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="dashboardPage" class="dashboard">
    <?php include 'includes/head.php' ;?>
     <div id="myModal" class="modal fade">
    <div class="modal-dialog modal-vertical-centered">
        <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Welcome to RightEverywhere.</h4>
                <P>Stay &amp; Grow. Supporting employees and their manager al al levels through a process of discovery to help them clarify their career value proposition and needs, and the align these with the organization to drive their career, thereby increasing alignment, engagement and productivity.</p>
                <p>
                <div class="left col-md-6 col-xs-12">
                  <ul>
                    <h6>Option 1 Left </h6>
                    <li>list item one</li>
                    <li>list item two</li>
                    <li>list item thres</li>
                  </ul>
                  <div class="modal_btn_wrp">
                 <button type="button" id="purchase1" class="orange button register">Purchase option one at $111</button>
                 </div>
                </div>
                 <div class="left col-md-6 col-xs-12">
                  <ul>
                    <h6>Option 2 Right </h6>
                    <li>list item one</li>
                    <li>list item two</li>
                    <li>list item thres</li>
                  </ul>
                  <div class="modal_btn_wrp">
                  <button type="button" id="purchase2" class="orange button register">Purchase option two at $123</button>
                  </div>
                </div>
              </p>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
       <!-- SIDEBAR MENU START -->
      <?php include 'includes/sidebar.php' ;?>
      <!-- SIDEBAR MENU END -->

      <!-- CONTAINER WRAP START -->
      <div class="container_wrp">
        <!-- HEADER BANNER START -->
        <div id="welcome_banner" class="milwaukee_morn">
          <h2 class="message">Good Morning Darrell</h2>
          <div class="location">Milwaukee, WI</div>
          <div class="date">July, 29 2014</div>
        </div>
        <!-- HOME BANNER END -->

        <!-- DASH WRAP START -->
        <div class="dash_wrp">
          <div class="row">

            <div class="dash_container">
              <div class="col-xs-12 col-sm-4 col-md-4 overall_prog hidden-xs">
                <div class="progressCircle">
                  <span>45<i>%</i></span>
                </div>
              </div>
              <!-- DASHBOARD PROGRESS -->
              <div class="col-xs-12 col-sm-7 col-md-8 right overall_prog">
                <h4>Overall Progress</h4>
                <div class="time_reminaing">You Have 4 days remaining!</div>
                <p>Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. Eu autem invidunt neglegentur pro, iusto audire platonem in nec.</p>
                <div class="col-xs-12 col-sm-12 col-md-6 p0">
                  <ul class="progress_list">
                    <li><a href="#"><span class="icon-discovery-icon icon-md left blue_txt" aria-hidden="true"></span>Self Discovery</a></li>
                    <li><a href="#"><span class="icon-career-planning-icon icon-md left green_txt" aria-hidden="true"></span>Career Planning</a></li>
                    <li><a href="#"><span class="icon-personal-branding-icon icon-md red_txt left" aria-hidden="true"></span>Personal Branding</a></li>
                  </ul>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 p0">
                  <ul class="progress_list">
                    <li><a href="#"><span class="icon-job-search-icon icon-md ltblue_txt left" aria-hidden="true"></span>Job Search</a></li>
                    <li><a href="#"><span class="icon-close-deal-icon icon-md ltgreen_txt left" aria-hidden="true"></span>Close the deal</a></li>
                    <li><a href="#" class="blue_txt"><span class="icon-time icon-md blue_txt left"></span>Extend My Program</a></li>
                  </ul>
                </div>
              </div>
              <!-- DASHBOARD PROGRESS END -->
            </div>

            <div class="dash_container">
              <div class="container_heading_sm">NEXT STEP: <span>Career Planning</span><span class="icon-career-planning-icon orange_txt icon-md right" aria-hidden="true"></span></div>
              <div class="p2">
                <div class="stepTitle">Identify Your Ideal Position</div>
                <p>Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. Eu autem invidunt neglegentur pro, iusto audire platonem in nec.</p>
                <div class="timeToTake">TIME TO TAKE: <span>Approximately 20 minutes</span></div>
                <a class="button orange" href="#">Get Started</a>
                <a href="#" class="link">SKIP TO: Look at Options  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
              </div>
            </div>

            <!-- START DASHBOARD EVENTS -->
            <div class="dash_container col-xs-12 col-sm-7-5 col-md-7-5 left">
             <h5>My Events <span class="icon-events-icon orange_txt" aria-hidden="true"></span></h5>
              <ul class="hm_events">
                <li class="purple">
                  <a href="#">
                    <div class="event_title">Delhaize America Employer Spotlight</div>
                    <div class="event_time">10:30 AM JULY 31, 2014</div>
                  </a>
                </li>
                <li class="red">
                  <a href="#">
                    <div class="event_title">Life Options - Retirement</div>
                    <div class="event_time">10:30 AM JULY 31, 2014</div>
                  </a>
                </li>
                <li class="blue">
                  <a href="#">
                    <div class="event_title">Advanced LinkedIn</div>
                    <div class="event_time">10:30 AM JULY 31, 2014</div>
                  </a>
                </li>
              </ul>
              <a href="#" class="all-events">View All Events</a>
            </div>
            <!-- END DASHBOARD EVENTS -->

            <!-- START DASHBOARD CAREER COACH -->
            <div class="dash_container col-xs-12 col-sm-4 col-md-4 right">
              <h5>Career Coach <span class="icon-career-coach-icon orange_txt" aria-hidden="true"></span></h5>
              <div class="p2" id="careerCoach">
                <div class="name" id="careerName">Han Solo</div>
                <div class="text">SPECIALTIES:<br><span id="specialties" >Captain of the Millennium Falcon</span></div>
                <div class="text">REGION:<br><span id="region">Corellia Galaxy</span></div>
              </div>
            </div>
            <!-- END DASHBOARD CAREER COACH -->

          </div><!-- END ROW -->
        </div><!-- END DASH WRAP -->
      </div><!-- END CONTAINER WRAP -->
      <?php include 'includes/footer.php' ;?>
    </div><!-- END MAIN WRAPPER -->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="js/circle-progress.js"></script>
    <script src="js/modernizr.custom.js"></script>
    <script src="js/dashboard.js"></script>
     <script>

        // WELCOME MODAL //

       function reposition() {
        var modal = $(this),
            dialog = modal.find('.modal-vertical-centered');
        modal.css('display', 'block');

        // Dividing by two centers the modal exactly, but dividing by three
        // or four works better for larger screens.
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
        }
        // Reposition when a modal is shown
        $('.modal').on('show.bs.modal', reposition);
        // Reposition when the window is resized
        $(window).on('resize', function() {
            $('.modal:visible').each(reposition);
        });


        $(window).load(function(){
        $('#myModal').modal('show');
        });


        // ANIMATES DASHBOARD PROGRESS CIRCLE //
        $('.progressCircle').circleProgress({
            value: 0.45,
            size: 220,
            startAngle: 4.7,
            fill: { gradient: ['#4a94eb', '#5d9c9a'] }
        }).on('circle-animation-progress', function(event, progress) {
            $(this).find('span').html(parseInt(45 * progress) + '<i>%</i>');
        });
     </script>
  </body>
</html>
