<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manpower RightEverywhere Sign in</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body id="signinPage">
    <div class="page_wrp">
      <div id="main">
      <!-- HOME WELCOME BOX -->
      <div class="welcome_bx row">
        <div class="col-xs-12 col-sm-6 col-lg-6 signinLogo"><img id="logo" src="images/logo.png" alt="Right Management Logo" /></div>
        <div class="col-xs-12 col-sm-4 col-lg-4 right">
        <label for="signinLanguage" class="hiddenLabel">Switch Language</label>

        <select class="language" id="signinLanguage">
          <option value="#">Switch Language</option>
          <option value="english">English</option>
          <option value="spanish">Spanish</option>
          <option value="sample">Sample</option>
        </select>
        </div>
        <div class="full-width">
          <h1>
            Career solutions right from your desktop. 
          </h1>   
          <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
              Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. 
              Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, 
              nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien.
          </p>
        </div>
      </div>
      <!-- END HOME WELCOME BOX -->

      <!-- SIGN IN BOX --> 
      <div class="signin_bx row">

        <div class="signIn_left col-xs-12 col-sm-6 col-lg-6">
          <div class="p2">
            <h2 class="si_title">Sign in to RightEverywhere</h2>
            <!-- SINGN IN --> 
            <div class="form-group has-feedback">
            <label for="signinUsername" class="hiddenLabel">Username</label>
            <input type="text" class="form-control" id="signinUsername" placeholder="Username" name="fname">
            </div><br>
            <div class="form-group has-feedback">
            <label for="signinPassword" class="hiddenLabel">Password</label>
            <input type="text" class="form-control" id="signinPassword" placeholder="Password" name="lname">
            </div><br> 
            
              <input type="checkbox" id="checkbox-3-1"><label class="checkbx" for="checkbox-3-1">Remember Me</label> <br>
            
            <span class="forgot">Forgot your <a href="#" id="username">username</a> or <a href="#" id="password">password</a></span>
            <input class="submit signinBTN" type="button" value="SIGN IN" onclick="submitTryit()">
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-6 signIn_right p2">
          <h3>We've helped over ## take the next step lorem ipsum dolor  sit amet.</h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
          </p>
          <p>
            Register now to sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis.
          </p>
          <h4 style="margin-top:3em;">Ready To Start?</h4>
          <input class="submit registerBTN col-xs-12" type="button" value="COMPLETE REGISTRATION">
        </div>
      </div>
      <!-- END SIGN IN BOX -->
    </div>
  </div>
    <footer class="signin navbar navbar-inverse navbar-bottom">
    <!-- INCLUDE FOOTER FILE 
    <?php include 'includes/footer.php' ;?>
    -->
    <ul class="footerNav"> 
      <li><a href="#">Contact Us</a></li>
      <li><a href="#">Your Data Privacy</a></li>
      <li><a href="#">Cookie Overview</a></li>
      <li><a href="#">Terms of Use</a></li>
    </ul>
    <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>

    </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   


      <script src="js/modernizr.custom.js"></script>
        <script>
            if (Modernizr.svg)
				{
					$("#logo").attr("src", "images/logo.svg");
					console.log('changed');
				};
        </script>
    
  </body>
</html>