<header class="header_wrp navbar navbar-default"  role="navigation">
 <a  href="#menu-toggle" id="menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
 </a>
 <div class="dash_logo">
   <img id="logo" src="images/right_logo.png" alt="Right Mangement Logo" />
 </div>

 <ul class="header_nav hidden-xs">
   <li><a href="#">Resources</a></li>
   <li><a href="#">Log Out</a></li>
 </ul>
 <a href="#" class="hidden-xs" id="messages">
   <span>3</span>
 </a>
</header>
