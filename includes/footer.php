
<footer class="dashfooter navbar hidden-xs">
  <div class="inner-footer">
    <div class="footer-logo">
      <img id="ft-logo" src="images/logo.png" alt="Right Mangement Logo" />
    </div>
    <ul class="footerNav">
      <li><a href="#">Contact Us</a></li>
      <li><a href="#">Your Data Privacy</a></li>
      <li><a href="#">Cookie Overview</a></li>
      <li><a href="#">Terms of Use</a></li>
    </ul>
    <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
  </div>
</footer>
