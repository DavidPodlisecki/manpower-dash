<div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
  <a href="#clost-menu-toggle" class="close-toggle" id="close-menu-toggle">X</a>
  <ul class="nav top-nav hidden-sm hidden-md hidden-lg">
    <li><a class="noICO" href="#">NOTIFICATIONS</a></li>
    <li><a class="noICO" href="#">RESOURCES</a></li>
    <li><a class="noICO" href="#">LOG OUT</a>
  </ul>
  <ul class="nav orange-icon" id="orange_icon">
    <li><div class="navTitle">MAIN</div></li>
    <li><a id="nav-dashboard" href="Dashboard.php"><span class="icon-dashboard-icon icon-md nav-color" aria-hidden="true"></span>Dashboard</a></li>
    <li><a id="nav-profile"href="Profile.php"><span class="icon-profile-icon icon-md nav-color" aria-hidden="true"></span>Profile</a></li>
    <li><a id="nav-coach"class="carrer_coach" href="#"><span class="icon-career-coach-icon icon-md nav-color" aria-hidden="true"></span>Career Coach</a>
      <ul>
        <li id="coach-info">
          <h4>Han Solo</h4>
          <div class="phone">123-123-1234</div>
          <div class="email">hansolo@galaxy.com</div>
        </li>
      </ul>
    </li>
    <li><a id="nav-events" href="Events.php"><span class="icon-events-icon icon-md nav-color" aria-hidden="true"></span>Events &amp; Webinars</a></li>
    <li><a id="nav-favorites" href="#"><span class="icon-favorite-icon icon-md nav-color" aria-hidden="true"></span>Favorites</a></li>
    <li><a id="nav-help" class="help" href="#"><span class="icon-help-icon icon-md nav-color" aria-hidden="true"></span>Help</a>
      <ul>
        <li id="help-info" class="help-desk">
          <h4>Help Desk</h4>
          <div class="p2">
            <p>For assistance with RightEverywhere technical questions and quick job search coaching questions, please call <strong>800.668.8556</strong> </p>
            <p>8AM- 8PM Eastern (Mon - Fri) <br>9AM- 5PM Eastern (Sat - Sun)</p>
          </div>
        </li>
      </ul>
    </li>
    <li><a id="nav-documents" href="#"><span class="icon-icon_document icon-md nav-color" aria-hidden="true"></span>Company Documents</a></li>
    <li><a id="nav-purchase" href="#"><span class="icon-icon_options icon-md nav-color" aria-hidden="true"></span>Purchase Options</a></li>
    <li><div class="navTitle">PROGRESS</div></li>
    <li><a id="nav-discovery" href="#"><span class="icon-discovery-icon icon-md nav-color" aria-hidden="true"></span>Self Discovery</a></li>
    <li><a id="nav-planning" href="#"><span class="icon-career-planning-icon icon-md nav-color" aria-hidden="true"></span>Career Planning</a></li>
    <li><a id="nav-branding" href="#"><span class="icon-personal-branding-icon icon-md nav-color" aria-hidden="true"></span>Personal Branding</a></li>
    <li><a id="nav-search"href="#"><span class="icon-job-search-icon icon-md nav-color" aria-hidden="true"></span>Job Search</a></li>
    <li><a id="nav-close" href="#"><span class="icon-close-deal-icon icon-md nav-color" aria-hidden="true"></span>Close the Deal</a></li>
  </ul>
  <div class="sb-footer navbar hidden-sm hidden-md hidden-lg">
    <ul class="footerNav ">
      <li><a href="#">Contact Us</a></li>
      <li><a href="#">Your Data Privacy</a></li>
      <li><a href="#">Cookie Overview</a></li>
      <li><a href="#">Terms of Use</a></li>
    </ul>
    <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
  </div>
</div>
