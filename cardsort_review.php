<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Manpower RightEverywhere </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/icon-style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="resumePage" class="dashboard">
    <!-- START HEADER -->
    <div class="header_wrp navbar navbar-default"  role="navigation">
      <a  href="#menu-toggle" id="menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </a>
      <div class="dash_logo">
        <img id="logo" src="images/right_logo.png" alt="Right Mangement Logo" />
      </div>
      
      <ul class="header_nav hidden-xs">
        <li><a href="#" >Resources</a></li>
        <li><a href="#">Log Out</a></li>
      </ul>
      <a href="#" class="hidden-xs" id="messages">
        <span>3</span>
      </a>
    </div>
    <!-- END HEADER -->
 
    <!-- START WRAPPER -->
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
      <!-- SIDEBAR MENU START -->
      <div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
        <a href="#clost-menu-toggle" class="close-toggle" id="close-menu-toggle">X</a>
        <ul class="nav top-nav hidden-sm hidden-md hidden-lg">
          <li><a class="noICO" href="#">NOTIFICATIONS</a></li>
          <li><a class="noICO" href="#">RESOURCES</a></li>
          <li><a class="noICO" href="#">LOG OUT</a>
        </ul>
        <ul class="nav orange-icon" id="orange_icon">
          <li><div class="navTitle">MAIN</div></li>
          <li><a href="#"><span class="icon-dashboard-icon icon-md nav-color" aria-hidden="true"></span>Dashboard</a></li>
          <li><a href="#"><span class="icon-profile-icon icon-md nav-color" aria-hidden="true"></span>Profile</a></li>
          <li><a class="carrer_coach" href="#"><span class="icon-career-coach-icon icon-md nav-color" aria-hidden="true"></span>Career Coach</a>
            <ul>
              <li id="coach-info">
                <h4>Han Solo</h4>
                <div class="phone">123-123-1234</div> 
                <div class="email">hansolo@galaxy.com</div>
              </li>
            </ul>
          </li>
          <li><a href="#"><span class="icon-events-icon icon-md nav-color" aria-hidden="true"></span>Events &amp; Webinars</a></li>
          <li><a href="#"><span class="icon-favorite-icon icon-md nav-color" aria-hidden="true"></span>Favorites</a></li> 
          <li><a class="help" href="#"><span class="icon-help-icon icon-md nav-color" aria-hidden="true"></span>Help</a>
            <ul>
              <li id="help-info" class="help-desk">
                <h4>Help Desk</h4>
                <div class="p2">
                  <p>For assistance with RightEverywhere technical questions and quick job search coaching questions, please call <strong>800.668.8556</strong> </p>
                  <p>8AM- 8PM Eastern (Mon - Fri) <br>9AM- 5PM Eastern (Sat - Sun)</p>
                </div>
              </li>
            </ul>
          </li>
          <li><div class="navTitle">PROGRESS</div></li>
          <li><a href="#"><span class="icon-discovery-icon icon-md nav-color" aria-hidden="true"></span>Self Discovery</a></li>
          <li><a href="#"><span class="icon-career-planning-icon icon-md nav-color" aria-hidden="true"></span>Career Planning</a></li>
          <li><a class="active" href="#"><span class="icon-personal-branding-icon icon-md nav-color" aria-hidden="true"></span>Personal Branding</a></li>
          <li><a href="#"><span class="icon-job-search-icon icon-md nav-color" aria-hidden="true"></span>Job Search</a></li>
          <li><a href="#"><span class="icon-close-deal-icon icon-md nav-color" aria-hidden="true"></span>Close the Deal</a></li>             
        </ul>
        <div class="sb-footer navbar hidden-sm hidden-md hidden-lg">
          <ul class="footerNav "> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </div>
      <!-- SIDEBAR MENU END -->
      
      
      <!-- START PAGE CONTAINER -->
      <div class="container_wrp" id="container_wrp">
        <div class="dash_wrp">
          <div class="row">
            <a href="#" class="left backBTN"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Back To Self Discovery</a>
            
            <!-- INTRO --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <div class="p2">
                <h4 class="resource blue_txt">Values &amp; Drivers: Review</h4>
                <div class="right col-xs-12 col-sm-12 col-md-12">
                  <div class="m1-0">
                    <p>
                    Based on your card sort, we’ve narrowed down your values and drivers. Quisque porttitor pulvinar diam id mattis. Pellentesque lacinia laoreet mi, at sodales leo volutpat at. Vivamus ex sapien, sagittis eu diam non, congue
                    </p>
                    
                  </div> 
                </div>
              </div>
            </div><!-- INTRO -->
          </div><!-- END ROW -->

          <div class="dash_container panel panel-DKblue col-xs-12 col-sm-5-5 col-md-5-5 left">
                <div class="panel-heading">
                  <h3 class="panel-title left-lower">Your Values</h3>
                </div>
                <p class="p2">
                    You value blah blah blah proin non semper lectus. Fusce cursus nunc ut dignissim tempor. Nam rutrum turpis nec metus fermentum interdum. Praesent tincidunt massa eget. mauris iaculis, aliquam auctor turpis porta.
                </p>

              </div>

          <div class="dash_container panel panel-DKblue col-xs-12 col-sm-5-5 col-md-5-5 right">
                <div class="panel-heading">
                  <h3 class="panel-title left-lower">Your Ideal Work Enviroment</h3>
                </div>
                <p class="p2">
                    People with these values often like work environments blah blah blah Proin non semper lectus. Fusce cursus nunc ut dignissim tempor. Nam rutrum turpis nec metus fermentum interdum. Praesent tincidunt massa eget.
                </p>

              </div>

                <div class="dash_container panel panel-blue col-xs-12 col-sm-12 col-md-12">
                <div class="panel-heading">
                  <h3 class="panel-title left-upper">ESSENTIAL</h3>
                </div>
                <p class="p2">
                    Alignment, accomplishment, creativity, challenge, alignment, accomplishment.
                </p>

              </div>
              <div class="dash_container panel panel-orange col-xs-12 col-sm-12 col-md-12">
                <div class="panel-heading">
                  <h3 class="panel-title left-upper">Important</h3>
                </div>
                  <p class="p2">
                    Affiliation, company size, personal space, vacation time.
                </p>
              </div>
              <div class="dash_container panel panel-green col-xs-12 col-sm-12 col-md-12">
                <div class="panel-heading">
                  <h3 class="panel-title left-upper">Nice To Have</h3>
                </div>
                  <p class="p2">
                    Alignment, accomplishment, creativity, challenge, alignment, accomplishment.
                </p>
              </div>
              <div class="dash_container panel panel-dkgray col-xs-12 col-sm-12 col-md-12">
                <div class="panel-heading">
                  <h3 class="panel-title left-upper">Not Important</h3>
                </div>
                  <p class="p2">
                    Gym, competition, hierarchy.
                </p>
              </div>
              <div class="dash_container panel panel-red col-xs-12 col-sm-12 col-md-12">
                <div class="panel-heading">
                  <h3 class="panel-title left-upper">Avoid</h3>
                </div>
                  <p class="p2">
                   Strict schedule, physical labor.
                </p>
              </div>


               <!-- CONTAINER --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
                <h5 class="lt_gray_txt">CAREERS THE MATCH YOUR SKILLS</h5>
                <div class="p1">
          <div class=" col-xs-12 col-sm-4 col-md-4 p1">
            Account Executive<br />
            Financial Advisor<br />
            Risk Assessor
          </div>
          <div class=" col-xs-12 col-sm-4 col-md-4 p1">
            Creative Director<br />
            Retail Manager
          </div>      

                </div>
            </div><!-- END CONTAINER-->


             <!-- START  --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
             
              <div class="p2">
                 <div class=" col-xs-12 col-sm-4 col-md-4">
                <a class="left mb2-0" href="#"><span class="icon-share-icon icon-md left" aria-hidden="true"></span><span class="lh2-2">Share With Coach</span></a>
              </div>
              <div class=" col-xs-12 col-sm-4 col-md-4">
                <a class="left mb2-0" href="#"><span class="icon-download-icon icon-md left" aria-hidden="true"></span><span class="lh2-2">Download as PDF</span></a>
              </div>
          
                <div class="right col-xs-12 col-sm-12 col-md-12">
                  <div class="col-xs-12 col-sm-12 col-lg-12 p0"> 
                   <a class="left black ls1 mb2-0" href="#"><span class="icon-back-to-icon icon-md left" aria-hidden="true"></span><span class="lh2-2">BACK TO SELF DISCOVERY</span></a>
                   <a class="orange button right next mb2" id="next2"  href="resume_builder_step7.php">NEXT ASSESSMENT</a> 
                  </div>
                   
                </div>
              </div>
            </div><!-- END --> 

        </div><!-- END DASH WRAP -->
      </div><!-- END PAGE CONTAINER -->     
      

      <footer class="dashfooter navbar hidden-xs">
      <!-- INCLUDE FOOTER IN FINAL CODE  < ?php include 'includes/footer.php' ;?> -->
        <div class="inner-footer">
          <div class="footer-logo">
            <img id="ft-logo" src="images/logo.png" alt="Right Mangement Logo" />
          </div>
          <ul class="footerNav"> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </footer> 
    </div><!-- END MAIN WRAPPER --> 
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   

      <script src="js/modernizr.custom.js"></script>
     
    <script>
        // SVG LOGO DETECT // 
        if (Modernizr.svg)
        {
          $("#logo").attr("src", "images/right_logo.svg");
          console.log('changed');

          $("#ft-logo").attr("src", "images/logo.svg");
          console.log('changed');
        }; 
       

        // MOBILE SIDE MENU TOGGLE // 
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

         $("#close-menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").removeClass("toggled");
        });
        
        // HIDES MOBILE SUB MENU //
        $(".carrer_coach").click(function(e) {
            e.preventDefault();
            $("#coach-info").toggleClass("invisible");
        });

        $(".help").click(function(e) {
            e.preventDefault();
            $("#help-info").toggleClass("invisible");
        });

        
     </script>
    

  </body>
</html>
