<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manpower RightEverywhere </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/icon-style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/animate.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="eventsPage" class="dashboard">
  <?php include 'includes/head.php' ;?>

    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
      <!-- SIDEBAR MENU START -->
      <?php include 'includes/sidebar.php' ;?>
      <!-- SIDEBAR MENU END -->

      <!-- CONTAINER WRAP START -->
      <div class="container_wrp">
        <div class="dash_wrp">
          <div class="row">
            <h1 class="icon event">Events &amp; Webinars</h1>
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
               <h4 class="blankTitle">MY EVENTS &amp; WEBINARS: <span> 6 registered</span></h4>
            </div>
          </div>
          <div class="row">
            <!-- EVENT LIST PAGINATION -->
            <ul id="itemContainer" class="col-xs-12">
              <!-- EVENT START -->
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp registered">
                <h5>The Pichbook - Game Changeing Interview Tool</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <!-- EVENT END -->
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp registered">
                <h5>Life Options: Retirement</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp registered">
                <h5>Vehicula Dapibus Purus Sem</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp registered">
                <h5>Dolor Condimentum Dapibus Inceptos</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp registered">
                <h5>The Pichbook - Game Changeing Interview Tool</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp registered">
                <h5>Life Options: Retirement</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp registered">
                <h5>Vehicula Dapibus Purus Sem</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp registered">
                <h5>Dolor Condimentum Dapibus Inceptos</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp registered">
                <h5>The Pichbook - Game Changeing Interview Tool</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp registered">
                  <h5>Life Options: Retirement</h5>
                  <div class="info">
                    <p>November 1, 2014</p>
                    <p>2:30-4pm</p>
                    <p>Location Name</p>
                    <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                  </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp registered">
                <h5>Vehicula Dapibus Purus Sem</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp registered">
                <h5>Dolor Condimentum Dapibus Inceptos</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp registered">
                <h5>The Pichbook - Game Changeing Interview Tool</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp registered">
                <h5>Life Options: Retirement</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp registered">
                <h5>Vehicula Dapibus Purus Sem</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp registered">
                <h5>Dolor Condimentum Dapibus Inceptos</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <a href="#" class="glyphicon glyphicon-remove-circle cancel">CANCEL</a>
                </div>
              </li>
            </ul>
            <div class="holder" id="holder1"></div>
          </div><!-- END ROW -->

          <div class="dash_container col-xs-12 col-sm-12 col-md-12" style="margin-top:4em;">
            <h4 class="blankTitle">IN OFFICE EVENTS: <span> 6 available</span></h4>
          </div>

          <div class="row">
            <ul id="itemContainer2" class="col-xs-12">
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp">
                <h5 class="red">The Pichbook - Game Changeing Interview Tool</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <span>You are registered for thie event</span>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp">
                <h5>Life Options: Retirement</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <div class="regBtn">
                    <!-- Button trigger modal -->
                    <button type="button" id="register" class="orange button register" data-toggle="modal" data-target="#myModal">REGISTER</button>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Life Options: Retirement</h4>
                        </div>
                        <div class="modal-body">
                          <div class="description">
                          <p><strong>Program Description:</strong> Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. </p>
                          <p><strong>Event Date:</strong> 10/9/2014 9:00 -11:00 AM</p>
                          <p><strong>Location:</strong> 1234 Street St. Milwaukee, WI 53202</p>
                          <p><strong>Facilitator:</strong> Laura Bixby, bio here Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. </p>
                          </div>
                           <button type="button" id="register" class="orange button register">Register</button>
                           <!-- RESULT AFTER USER CLICKS REGISTER
                           <div class="reg-result">Thank you for registering for this event</div>
                            -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- END MODAL -->
                </div>
              </li>

              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp">
                <h5 class="red">The Pichbook - Game Changeing Interview Tool</h5>
                  <div class="info">
                    <p>November 1, 2014</p>
                    <p>2:30-4pm</p>
                    <p>Location Name</p>
                    <span>You are registered for thie event</span>
                  </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp">
                <h5>Life Options: Retirement</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <div class="regBtn">
                    <!-- Button trigger modal -->
                    <button type="button" id="register" class="orange button register" data-toggle="modal" data-target="#myModal12">REGISTER</button>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade" id="myModal12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Life Options: Retirement</h4>
                        </div>
                        <div class="modal-body">
                          <div class="description">
                          <p><strong>Program Description:</strong> Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. </p>
                          <p><strong>Event Date:</strong> 10/9/2014 9:00 -11:00 AM</p>
                          <p><strong>Location:</strong> 1234 Street St. Milwaukee, WI 53202</p>
                          <p><strong>Facilitator:</strong> Laura Bixby, bio here Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. </p>
                          </div>
                           <div class="reg-result">Thank you for registering for this event</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- END MODAL -->
                </div>
              </li>
            </ul>
            <div class="holder" id="holder2"></div>
          </div><!-- END ROW -->
          <!-- END IN OFFICE EVENTS -->

          <!-- WEBINAR START -->
          <div class="dash_container col-xs-12 col-sm-12 col-md-12" style="margin-top:4em;">
            <h4 class="blankTitle">WEBINARS: <span> 30 available</span></h4>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="dropdown right">
                <button id="dLabel" class="orange_INV button register" type="button " data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"><div class="btn-text">All</div><span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                  <li><a href="#">Category 1 </a></li>
                  <li><a href="#">Category 2 </a></li>
                  <li><a href="#">Category 1 </a></li>
                  <li><a href="#">Category 2 </a></li>
                </ul>
              </div>
            </div>
            <ul id="itemContainer3" class="col-xs-12">
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp">
                <h5 class="blue">Life Options: Retirement</h5>
                  <div class="info">
                    <p>November 1, 2014</p>
                    <p>2:30-4pm</p>
                    <p>Location Name</p>
                    <div class="regBtn">
                      <!-- Button trigger modal -->
                      <button type="button" class="orange button register" data-toggle="modal" data-target="#myModal_1">VIEW TIMES</button>
              </li>
              <!-- Modal -->
              <div class="modal fade" id="myModal_1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <h4 class="modal-title" id="myModalLabel">Life Options: Retirement</h4>
                    </div>
                    <div class="modal-body">
                      <div class="description">
                      <p>
                        Join us for introduction and orientation on social media and the positive impact it can have as part of your overall campaign strategy. Learn how to leverage the power of social media and marketing using LinkedIn, Facebook and Twitter in an effective job search. Understand how employees and recruiters are using these online communities to search and fill their needs. We will also discuss building your personal brand and creating an online presence while protecting your privacy and managing your online reputation.
                      </p>
                      <ul class="webinars">
                        <li>
                          <div class="time"><strong>10/1/2014</strong> 10:00AM - 12:00PM </div>
                          <div class="presenter">Danielle Moser</div>
                          <div class="register"><button type="button" class="orange button register">Register</button></div>
                        </li>
                         <li>
                          <div class="time"><strong>10/14/2014</strong> 11:00AM - 1:00PM </div>
                          <div class="presenter">Nancy Bremhorst</div>
                          <div class="register"><button type="button" class="orange button register">Register</button></div>
                        </li>
                         <li>
                          <div class="time"><strong>10/22/2014</strong> 2:00PM - 4:00PM </div>
                          <div class="presenter">Susan Morales</div>
                          <div class="register"><button type="button" class="orange button register">Register</button></div>
                        </li>
                         <li>
                          <div class="time"><strong>11/3/2014</strong> 3:00PM - 5:00PM </div>
                          <div class="presenter">Renee Zung</div>
                          <div class="register"><button type="button" class="orange button register">Register</button></div>
                        </li>
                         <li>
                          <div class="time"><strong>11/5/2014</strong> 9:00AM - 11:00AM </div>
                          <div class="presenter">Pamela Sanchez</div>
                          <div class="register"><button type="button" class="orange button register">Register</button></div>
                        </li>
                         <li>
                          <div class="time"><strong>11/16/2014</strong> 4:00AM - 6:00PM </div>
                          <div class="presenter">Jody Fosnough</div>
                          <div class="register"><button type="button" class="orange button register">Register</button></div>
                        </li>
                        <li>
                          <div class="time"><strong>11/21/2014</strong> 1:30PM - 3:30PM </div>
                          <div class="presenter">Michelle Newhouse</div>
                          <div class="register"><button type="button" class="orange button register">Register</button></div>
                        </li>
                        <li>
                          <div class="time"><strong>11/24/2014</strong> 10:00AM - 12:00PM </div>
                          <div class="presenter">Michelle Newhouse</div>
                          <div class="register"><button type="button" class="orange button register">Register</button></div>
                        </li>
                      </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END MODAL -->

              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp">
                <h5 class="red">The Pichbook - Game Changeing Interview Tool</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <span>You are registered for thie event</span>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp">
                <h5>The Pichbook - Game Changeing Interview Tool</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <div class="regBtn"><a href="#" class="orange button register">VIEW TIMES</a></div>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp">
                <h5 class="green">Life Options: Retirement</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <div class="regBtn"><a href="#" class="orange button register">VIEW TIMES</a></div>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 left event_wrp">
                <h5>The Pichbook - Game Changeing Interview Tool</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <div class="regBtn"><a href="#" class="orange button register">VIEW TIMES</a></div>
                </div>
              </li>
              <li class="dash_container col-xs-12 col-sm-5-5 col-md-5-5 right event_wrp">
                <h5 class="green">Life Options: Retirement</h5>
                <div class="info">
                  <p>November 1, 2014</p>
                  <p>2:30-4pm</p>
                  <p>Location Name</p>
                  <div class="regBtn"><a href="#" class="orange button register">VIEW TIMES</a></div>
                </div>
              </li>
            </ul>
            <div class="holder" id="holder3"></div>
          </div><!-- END ROW -->
        </div><!--END DASH WRP -->
      </div><!-- END CONTAINER WRAP -->

      <?php include 'includes/footer.php' ;?>
    </div><!-- END MAIN WRAPPER -->


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

      <script src="js/modernizr.custom.js"></script>
      <script src="js/jPages.js"></script>
      <script src="js/dashboard.js"></script>
      <script>
         /* when document is ready */
        $(function(){
          /* initiate the plugin */
          $("div#holder1").jPages({
            containerID  : "itemContainer",
            perPage      : 2,
            startPage    : 1,
            startRange   : 1,
            midRange     : 3,
            endRange     : 1,
            animation    : "fadeIn",
            previous     : "back"
          });
        });
      $(function(){
        /* initiate the plugin */
        $("div#holder2").jPages({
          containerID  : "itemContainer2",
          perPage      : 2,
          startPage    : 1,
          startRange   : 1,
          midRange     : 3,
          endRange     : 1,
          previous     : "back"
        });
      });
      $(function(){
        /* initiate the plugin */
        $("div#holder3").jPages({
          containerID  : "itemContainer3",
          perPage      : 6,
          startPage    : 1,
          startRange   : 1,
          midRange     : 3,
          endRange     : 1,
          previous     : "back"
        });
      });
      $('.dropdown-toggle').dropdown()
    </script>
  </body>
</html>
