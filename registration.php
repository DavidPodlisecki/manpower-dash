<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manpower RightEverywhere Registration</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="registrationPage" class="dashboard">
    
    <div class="main_wrp">
    <!-- INCLUDE HEADER FILE IN FINAL VERSION 
      <?php include 'includes/blank_dash_header.php' ?>
    -->
      <div class="header_wrp">
        <div class="dash_logo">
          <img id="logo" src="images/right_logo.png" alt="Right Mangement Logo" />
        </div>
      </div>
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
      <!-- SIDEBAR MENU START -->
      <div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
      </div>
    <!-- CONTAINER WRAP START -->
    <div class="container_wrp"> 
    <div class="reg_container">
      <div id="compleate" class="p2">
        <h1>Complete Your Registration</h1>
        <p>Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. </p>
      </div>
      
      <!-- REGISTRATION FORM START -->
      <div class="registraion_form">
        <!-- STEP PROCESS -->
        <div class="row">
        <ul id="step" class="col-xs-12 col-sm-12 col-lg-12">
            <li id="regStep1" class="active col-xs-12 col-md-4"><div class="circle">1</div>Enter Provied Info</li>
            <li id="regStep2" class="col-xs-12 col-md-4 "><div class="circle">2</div>Enter Personal Info</li>
            <li id="regStep3" class="col-xs-12 col-md-4 "><div class="circle">3</div> Review </li>
        </ul>
      </div>
        <!-- END STEP PROCESS -->

        <!-- STEP ONE -->
        <div id="step1" class="p2">

          <div class="reg_headWRP">
            <h4>Please enter the registration code you received by email.</h4>

          </div>
            <div class="form-group has-success has-feedback">
            <label for="rcode" class="hiddenLabel">Registraion Code</label>
            <input type="text" class="regCode" value="Registration Code" id="rcode" name="rcode">
            </div>
            <div class="reg_bntWRP">
              <input class="continue contBTN col-xs-12 col-sm-4 col-lg-4" type="button" value="Continue" > <input class="cancel canBTN col-xs-12 col-sm-4 col-lg-4" type="button" value="Cancel" >
            </div>
        </div>
        <!-- END STEP ONE -->

        <!-- STEP TWO -->
        <div id="step2" class="p2">
          <div class="reg_headWRP">
            <h4>Please enter the following information.</h4>
          </div>
          <div class="row">
            <form>
             <div class="left mr4 col-xs-12 col-sm-5 col-lg-5">
              <div class="form-group has-feedback">
              <label for="firstName">First Name</label>
              <input type="text" class="form-control required fname" id="firstName" name="fname" data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." type="text">
              <!-- <span class="glyphicon glyphicon-ok form-control-feedback"></span> -->                       
              </div>
              <div class="form-group has-feedback">
              <label for="lastName">Last Name</label>
              <input type="text" class="form-control required lname" id="lastName" name="lname" data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." type="text">
              <!-- <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span> -->           
              </div>
              <div class="form-group has-feedback">

              <label for="emailAddress">Email Address</label>
              <input type="text" class="form-control required email" id="emailAddress" name="eAddress" data-placement="top" data-trigger="manual" data-content="Must be a valid e-mail address (user@gmail.com)" type="text">
              <!-- <span class="glyphicon glyphicon-remove form-control-feedback"></span> -->          
              </div>
              <label for="pLanguage">Preferred Language</label>
              <select class="regLanguage required" name="pLanguage" id="pLanguage" data-placement="top" data-trigger="manual" data-content="You must choose a gender">
                <option value="english">English</option>
                <option value="spanish">Spanish</option>
                <option value="sample">Sample</option>
              </select>
              <label class="serviceOffice">Service Office <div class="smCircle"><button type="button" data-toggle="popover" title="Popover title" data-placement="bottom" data-content="Maecenas faucibus mollis interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.">?</button></div><br>
              <span>Please select from the offices nearest to you:</span>
              </label>
              <div class="radio">
                <input id="office1" name="office" type="radio" value="office1">
                <label for="office1" class="radiobx" >Office Name <br> 1234 Main St.<br>City, ST 53827</label>

                <input id="office2" name="office" type="radio" value="office2">
                <label for="office2" class="radiobx" >Office Name <br> 1234 Main St.<br>City, ST 53827</label>

                <input id="office3" name="office" type="radio" value="office3">
                <label for="office3" class="radiobx" >Office Name <br> 1234 Main St.<br>City, ST 53827</label>
              </div>
            </div>
            <div class="left  col-xs-12 col-sm-5 col-lg-5">
              <div class="form-group has-feedback">
              <label for="userName">Username (this will be your login)</label>
              <input type="text" class="form-control" id="lastName" name="userName">
              </div>
              <div class="form-group has-feedback">
              <label for="password">Password</label>
              <input type="text" class="form-control required pass" id="password" name="password" data-placement="top" data-trigger="manual" data-content="Must be at least 6 characters long, and contain at least one number, one uppercase and one lowercase letter." type="password">
              </div>
              <div class="form-group has-feedback">
              <label for="ConfirmPassword">Confirm Password</label>
              <input type="text" class="form-control" id="ConfirmPassword" name="ConfirmPassword" data-placement="top" data-trigger="manual" data-content="Passwords must match"type="password">
              </div>

            </div>

            <!-- REMOVE BUTTON IN FINAL CODE, THIS VALIDATION BUTTON ONLY DEMO -->
            <div class="form-group col-xs-12 col-lg-12"><button type="submit" class="btn btn-default pull-left">Validation Demo (remove in final)</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
            <!-- END == REMOVE CODE ABOVE -->

            <div class="reg_bntWRP">
              <input class="continue contBTN col-xs-12 col-sm-4 col-lg-4" type="button" value="Continue" > <input class="cancel canBTN col-xs-12 col-sm-4 col-lg-4" type="button" value="Cancel" >
            </div>
            </form>
          </div>
        </div>
        <!-- END STEP TWO -->

        <!-- STEP THREE -->
        <div id="step3" class="p2">
          <div class="reg_headWRP">
            <h4>Please review our privacy policy. Registration cannot be complete with acceptance.</h4>
          </div>
          <div class="policy_bx">
            <p>PRIVACY POLICY Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam quis risus eget urna mollis ornare vel eu leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae elit libero, a pharetra augue. Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            <p>Cras mattis consectetur purus sit amet fermentum. Etiam porta sem malesuada magna mollis euismod. Nullam quis risus eget urna mollis ornare vel eu leo. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
            <p>Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.<p>
            <p>Maecenas sed diam eget risus varius blandit sit amet non magna. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas faucibus mollis interdum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
            <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec sed odio dui.</p>
            <p>Nulla vitae elit libero, a pharetra augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>            <p>Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
            <p>Donec ullamcorper nulla non metus auctor fringilla. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec id elit non mi porta gravida at eget metus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam id dolor id nibh ultricies vehicula ut id elit. Nulla vitae elit libero, a pharetra augue.</p>
          </div>
          <div class="checkbox">
            <input type="checkbox" id="checkbox-3-1" /><label class="checkbx" for="checkbox-3-1" style="margin-top:2em;">I have read and accept the terms of the privacy policy.</label>
          </div>
          <div class="reg_bntWRP">
            <input class="continue contBTN col-xs-12 col-sm-4 col-lg-4" type="button" value="Continue" > <input class="cancel canBTN col-xs-12 col-sm-4 col-lg-4" type="button" value="Cancel" >
          </div>
        </div>
        <!-- END STEP THREE -->

        <!-- STEP FOUR -->  
        <div id="step4" class="p2">
          <div class="reg_headWRP">
            <h1>Thank you for registering.</h1>
            <h4>An email to confirm that you have registered has been sent to your inbox. You will need to confirm this email to continue.</h4>
          </div>
          <p>Donec ullamcorper nulla non metus auctor fringilla. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec id elit non mi porta gravida at eget metus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam id dolor id nibh ultricies vehicula ut id elit. Nulla vitae elit libero, a pharetra augue.</p>
        </div>
        <!-- END STEP FOUR -->
      
      </div> 
      <!-- END STEP FOUR -->
    </div> 
  </div>
<footer class="dashfooter navbar">
      <!-- INCLUDE FOOTER IN FINAL CODE  < ?php include 'includes/footer.php' ;?> -->
        <div class="inner-footer">
          <div class="footer-logo">
            <img id="logo" src="images/logo.png" alt="Right Mangement Logo" />
          </div>
          <ul class="footerNav"> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </footer> 
    </div><!-- END MAIN WRAPPER --> 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   


      <script src="js/modernizr.custom.js"></script>
     
    <script>
        // SVG LOGO DETECT // 
        if (Modernizr.svg)
        {
          $("#logo").attr("src", "images/right_logo.svg");
          console.log('changed');

          $("#ft-logo").attr("src", "images/logo.svg");
          console.log('changed');
        }; 


    // FORM VALIDATION // 


    $.fn.goValidate = function() {
    var $form = this,
        $inputs = $form.find('input:text, input:password'),
        $selects = $form.find('select'),
        $textAreas = $form.find('textarea');
  
    var validators = {
        name: {
            regex: /^[A-Za-z]{3,}$/
        },
        username: {
            regex: /^[A-Za-z]{6,}$/
        },
        firstName: {
            regex: /^[A-Za-z]{3,}$/
        },
        lastName: {
            regex: /^[A-Za-z]{3,}$/
        },
        town: {
            regex: /^[A-Za-z]{3,}$/
        },
        postcode: {
            regex: /^.{3,}$/
        },
        password1: {
            regex: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/
        },
        password1_repeat: {
            regex: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/
        },
        email: {
            regex: /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/
        },
        phone: {
            regex: /^[2-9]\d{2}-\d{3}-\d{4}$/,
        },
        body: {
            regex: /^.{3,}$/
        },
        country: {
            regex: /^(?=\s*\S).*$/,
        }
    };
    var validate = function(klass, value) {
        var isValid = true,
            error = '';
            
        if (!value && /required/.test(klass)) {
            error = 'This field is required';
            isValid = false;
        } else {
            klass = klass.split(/\s/);
            $.each(klass, function(i, k){
                if (validators[k]) {
                    if (value && !validators[k].regex.test(value)) {
                        isValid = false;
                        error = validators[k].error;
                    }
                }
            });
        }
        return {
            isValid: isValid,
            error: error
        }
    };
    var showError = function($e) {
        var klass = $e.attr('class'),
            value = $e.val(),
            test = validate(klass, value);
        $e.removeClass('has-error');
        $e.parent().removeClass('has-error');
        $('#form-error').addClass('hide');
        
        if (!test.isValid) {
            $e.addClass('has-error');
           $e.parent().addClass('has-error');
            
            if(typeof $e.data("shown") == "undefined" || $e.data("shown") == false){
               $e.popover('show');
            }
            
        }
      else {
        $e.popover('hide');
      }
    };
   
    $inputs.keyup(function() {
        showError($(this));
    });
    $selects.change(function() {
        showError($(this));
    });
    $textAreas.keyup(function() {
        showError($(this));
    });
  
    $inputs.on('shown.bs.popover', function () {
      $(this).data("shown",true);
  });
  
    $inputs.on('hidden.bs.popover', function () {
      $(this).data("shown",false);
  });
  
    $form.submit(function(e) {
      
        $inputs.each(function() { /* test each input */
          if ($(this).is('.required') || $(this).hasClass('has-error')) {
              showError($(this));
          }
      });
      $selects.each(function() { /* test each input */
          if ($(this).is('.required') || $(this).hasClass('has-error')) {
              showError($(this));
          }
      });
      $textAreas.each(function() { /* test each input */
          if ($(this).is('.required') || $(this).hasClass('has-error')) {
              showError($(this));
          }
      });
        if ($form.find('input.has-error').length) { /* form is not valid */
          e.preventDefault();
            $('#form-error').toggleClass('hide');
        }
    });
    return this;
};

 $('form').goValidate();

 $('[data-toggle="popover"]').popover();

    $(function() {
      $('#step2').hide();
      $('#step3').hide();
      $('#step4').hide();
      
      $('#step1 .continue').click(function(){
      $('#step1').fadeTo( 'fast' , 0) .hide();
      $('#regStep1') .removeClass ('active');
      $('#regStep2') .addClass ('active');
      $("#step2").fadeTo( 'fast' , 1);  
    });

      $('#step2 .continue').click(function(){
      $('#step2').fadeTo( 'fast' , 0) .hide();
      $('#regStep2') .removeClass ('active');
      $('#regStep3') .addClass ('active');
      $('#step3').fadeTo( 'fast' , 1);  
      });
      
      $('#step3 .continue').click(function(){
      $('#step3').fadeTo( 'fast' , 0) .hide();
      $('#compleate').fadeTo( 'fast' , 0) .hide();
      $('#step').fadeTo( 'fast' , 0) .hide();
      $('#step4').fadeTo( 'fast' , 1);  
      });

        
 
    
    });
</script>

</div>

</div>
  </body>
  </html>
