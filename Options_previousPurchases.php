<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manpower RightEverywhere </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet"> 
    <link href="css/icon-style.css" rel="stylesheet"> 
    <link rel="stylesheet" href="css/animate.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="eventsPage" class="dashboard">
  <div class="header_wrp navbar navbar-default"  role="navigation">
      <a  href="#menu-toggle" id="menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </a>
      <div class="dash_logo">
        <img id="logo" src="images/right_logo.png" alt="Right Mangement Logo" />
      </div>
      
      <ul class="header_nav hidden-xs">
        <li><a href="#">Resources</a></li>
        <li><a href="#">Log Out</a></li>
      </ul>
      <a href="#" class="hidden-xs" id="messages">
        <span>3</span>
      </a>
    </div>    
    
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
      <!-- SIDEBAR MENU START -->
      <div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
        <a href="#clost-menu-toggle" class="close-toggle" id="close-menu-toggle">X</a>
        <ul class="nav top-nav hidden-sm hidden-md hidden-lg">
          <li><a class="noICO" href="#">NOTIFICATIONS</a></li>
          <li><a class="noICO" href="#">RESOURCES</a></li>
          <li><a class="noICO" href="#">LOG OUT</a>
        </ul>
        <ul class="nav orange-icon" id="orange_icon">
          <li><div class="navTitle">MAIN</div></li>
          <li><a href="#"><span class="icon-dashboard-icon icon-md nav-color" aria-hidden="true"></span>Dashboard</a></li>
          <li><a href="#"><span class="icon-profile-icon icon-md nav-color" aria-hidden="true"></span>Profile</a></li>
          <li><a class="carrer_coach" href="#"><span class="icon-career-coach-icon icon-md nav-color" aria-hidden="true"></span>Career Coach</a>
            <ul>
              <li id="coach-info">
                <h4>Han Solo</h4>
                <div class="phone">123-123-1234</div> 
                <div class="email">hansolo@galaxy.com</div>
              </li>
            </ul>
          </li>
          <li><a href="#"><span class="icon-events-icon icon-md nav-color" aria-hidden="true"></span>Events &amp; Webinars</a></li>
          <li><a href="#"><span class="icon-favorite-icon icon-md nav-color" aria-hidden="true"></span>Favorites</a></li> 
          <li><a class="help" href="#"><span class="icon-help-icon icon-md nav-color" aria-hidden="true"></span>Help</a>
            <ul>
              <li id="help-info" class="help-desk">
                <h4>Help Desk</h4>
                <div class="p2">
                  <p>For assistance with RightEverywhere technical questions and quick job search coaching questions, please call <strong>800.668.8556</strong> </p>
                  <p>8AM- 8PM Eastern (Mon - Fri) <br>9AM- 5PM Eastern (Sat - Sun)</p>
                </div>
              </li>
            </ul>
          </li>
          <li><a class="active" href="#"><span class="icon-icon_options icon-md nav-color" aria-hidden="true"></span>Purchase Options</a></li>
          <li><div class="navTitle">PROGRESS</div></li>
          <li><a href="#"><span class="icon-discovery-icon icon-md nav-color" aria-hidden="true"></span>Self Discovery</a></li>
          <li><a href="#"><span class="icon-career-planning-icon icon-md nav-color" aria-hidden="true"></span>Career Planning</a></li>
          <li><a href="#"><span class="icon-personal-branding-icon icon-md nav-color" aria-hidden="true"></span>Personal Branding</a></li>
          <li><a href="#"><span class="icon-job-search-icon icon-md nav-color" aria-hidden="true"></span>Job Search</a></li>
          <li><a href="#"><span class="icon-close-deal-icon icon-md nav-color" aria-hidden="true"></span>Close the Deal</a></li>             
        </ul>
        <div class="sb-footer navbar hidden-sm hidden-md hidden-lg">
          <ul class="footerNav "> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </div>
      <!-- SIDEBAR MENU END -->
      
      <!-- CONTAINER WRAP START -->
      <div class="container_wrp">
        <div class="dash_wrp">
          <a href="Options.php" class="left backBTN"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Back to Purchase Options</a>
          <h1><span class="icon-icon_options orange_txt" aria-hidden="true"></span>Purchase Options</h1>
          <div class="dash_container col-xs-12 col-sm-12 col-md-12" style="margin-top:4em;">
            <h4 class="blankTitle p_5_2 ">PREVIOUS PURCHASES</h4>
          </div>
        
          <div class="row">
            <ul id="itemContainer2" class="col-xs-12">
              <li class="dash_container col-xs-12 col-sm-3-5 col-md-3-5 options_wrp_NoMargin">
                <h5>Coaching</h5>
                <div class="info">
                  <p>Discription od this option lorem ipsum dolor sit amet<br><span class="blue_txt">10 Days Remaining</span></p>
                  <div class="regBtn">
                    <!-- Button trigger modal -->
                    <button type="button" id="register" class="orange button register" data-toggle="modal" data-target="#myModal">SEE MORE</button>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title blue_txt" id="myModalLabel">Name of the option</h4>
                        </div>
                        <div class="modal-body modal-options">
                          <div class="description">
                          <p><strong>Program Description:</strong> Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. </p>
                          <P>Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum.</p>
                          <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum id ligula porta felis euismod semper.</p>
                          </div>
                           
                           <!-- RESULT AFTER USER CLICKS REGISTER
                           <div class="reg-result">Thank you for registering for this event</div>
                            -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- END MODAL -->
                </div> 
              </li>

              <li class="dash_container col-xs-12 col-sm-3-5 col-md-3-5 options_wrp">
                <h5>Workfolio</h5>
                <div class="info">
                  <p>Discription od this option lorem ipsum dolor sit amet<br><span class="blue_txt">3 Days Remaining</span></p>

                  <div class="regBtn">
                    <!-- Button trigger modal -->
                    <button type="button" id="register" class="orange button register" data-toggle="modal" data-target="#myModal">SEE MORE</button>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title blue_txt" id="myModalLabel">Name of the option</h4>
                        </div>
                        <div class="modal-body modal-options">
                          <div class="description">
                          <p><strong>Program Description:</strong> Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. </p>
                          <P>Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum.</p>
                          <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum id ligula porta felis euismod semper.</p>
                          </div>
                           
                           <!-- RESULT AFTER USER CLICKS REGISTER
                           <div class="reg-result">Thank you for registering for this event</div>
                            -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- END MODAL -->
                </div> 
              </li>

              <li class="dash_container col-xs-12 col-sm-3-5 col-md-3-5 options_wrp_NoMargin">
                <h5>Birkman</h5>
                <div class="info">
                  <p>Discription od this option lorem ipsum dolor sit amet consectucar.</p>
                  <div class="regBtn">
                    <!-- Button trigger modal -->
                    <button type="button" id="register" class="orange button register" data-toggle="modal" data-target="#myModal">SEE MORE</button>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title blue_txt" id="myModalLabel">Name of the option</h4>
                        </div>
                        <div class="modal-body modal-options">
                          <div class="description">
                          <p><strong>Program Description:</strong> Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. </p>
                          <form class="form-horizontal">
                            <div class="control-group">
                              <div class="controls">
                                <p>
                                <label class="checkbox">
                                  <input 
                                    type="checkbox" 
                                    name="some_field" 
                                    data-validation-minchecked-minchecked="1" 
                                    data-validation-minchecked-message="Oops! You forgot to select a unit." 
                                  /> <strong>Unit Option 1:</strong> Sit Sollicitudin Cursus Parturient Purus <span>$24</span><br>
                                </label>
                                <label class="checkbox">
                                  <input type="checkbox" name="some_field" /> <strong>Unit Option 2:</strong> Sit Sollicitudin Cursus Parturient Purus <span>$32</span><br>
                                </label>
                                <label class="checkbox">
                                  <input type="checkbox" name="some_field" />  <strong>Unit Option 3:</strong> Sit Sollicitudin Cursus Parturient Purus <span>$26</span><br>
                                </label>
                                </p>
                                <button type="submit" id="register" class="orange button register">Purchase</button>
                                <p class="help-block"></p>
                              </div>
                            </div>
                          </form>
                          </div>
                           
                           <!-- RESULT AFTER USER CLICKS REGISTER
                           <div class="reg-result">Thank you for registering for this event</div>
                            -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- END MODAL -->
                </div> 
              </li>
              <li class="dash_container col-xs-12 col-sm-3-5 col-md-3-5 options_wrp_NoMargin">
                <h5>iView</h5>
                <div class="info">
                  <p>Discription od this option lorem ipsum dolor sit amet<br><span class="blue_txt">4 Days Remaining</span></p>
                  <div class="regBtn">
                    <!-- Button trigger modal -->
                    <button type="button" id="register" class="orange button register" data-toggle="modal" data-target="#myModal">SEE MORE</button>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title blue_txt" id="myModalLabel">Name of the option</h4>
                        </div>
                        <div class="modal-body modal-options">
                          <div class="description">
                          <p><strong>Program Description:</strong> Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. </p>
                          <form class="form-horizontal">
                            <div class="control-group">
                              <div class="controls">
                                <p>
                                <label class="checkbox">
                                  <input 
                                    type="checkbox" 
                                    name="some_field" 
                                    data-validation-minchecked-minchecked="1" 
                                    data-validation-minchecked-message="Oops! You forgot to select a unit." 
                                  /> <strong>Unit Option 1:</strong> Sit Sollicitudin Cursus Parturient Purus <span>$24</span><br>
                                </label>
                                <label class="checkbox">
                                  <input type="checkbox" name="some_field" /> <strong>Unit Option 2:</strong> Sit Sollicitudin Cursus Parturient Purus <span>$32</span><br>
                                </label>
                                <label class="checkbox">
                                  <input type="checkbox" name="some_field" />  <strong>Unit Option 3:</strong> Sit Sollicitudin Cursus Parturient Purus <span>$26</span><br>
                                </label>
                                </p>
                                <button type="submit" id="register" class="orange button register">Purchase</button>
                                <p class="help-block"></p>
                              </div>
                            </div>
                          </form>
                          </div>
                           
                           <!-- RESULT AFTER USER CLICKS REGISTER
                           <div class="reg-result">Thank you for registering for this event</div>
                            -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- END MODAL -->
                </div> 
              </li>
              <li class="dash_container col-xs-12 col-sm-3-5 col-md-3-5 options_wrp">
                <h5>Career Expo</h5>
                <div class="info">
                  <p>Discription od this option lorem ipsum dolor sit amet consectucar.</p>
                  <div class="regBtn">
                    <!-- Button trigger modal -->
                    <button type="button" id="register" class="orange button register" data-toggle="modal" data-target="#myModal">SEE MORE</button>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Name of the option</h4>
                        </div>
                        <div class="modal-body">
                          <div class="description">
                          <p><strong>Program Description:</strong> Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. </p>
                          <form>
                            <p>
                          <input type="radio" name="r-btn" value="r-btn1"> <strong>Unit Option 1:</strong>: Sit Sollicitudin Cursus Parturient Purus <span>$24</span><br>
                          <input type="radio" name="r-btn" value="r-btn2"> <strong>Unit Option 2:</strong>: Sit Sollicitudin Cursus Parturient Purus <span>$32</span><br>
                          <input type="radio" name="r-btn" value="r-btn3"> <strong>Unit Option 3:</strong>: Sit Sollicitudin Cursus Parturient Purus <span>$26</span><br>
                          </p>
                          </form>
                          </div>
                           <button type="button" id="register" class="orange button register">Register</button>
                           <!-- RESULT AFTER USER CLICKS REGISTER
                           <div class="reg-result">Thank you for registering for this event</div>
                            -->
                        </div>
                      </div>
                    </div>
                    </div>
                  </div>
                  <!-- END MODAL -->
                </div> 
              </li>
              <li class="dash_container col-xs-12 col-sm-3-5 col-md-3-5 options_wrp_NoMargin">
                <h5>Additional Item</h5>
                <div class="info">
                  <p>Discription od this option lorem ipsum dolor sit amet consectucar.</p>
                  <div class="regBtn">
                    <!-- Button trigger modal -->
                    <button type="button" id="register" class="orange button register" data-toggle="modal" data-target="#myModal">SEE MORE</button>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title blue_txt" id="myModalLabel">Name of the option</h4>
                        </div>
                        <div class="modal-body modal-options">
                          <div class="description">
                          <p><strong>Program Description:</strong> Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. </p>
                          <form class="form-horizontal">
                            <div class="control-group">
                              <div class="controls">
                                <p>
                                <label class="checkbox">
                                  <input 
                                    type="checkbox" 
                                    name="some_field" 
                                    data-validation-minchecked-minchecked="1" 
                                    data-validation-minchecked-message="Oops! You forgot to select a unit."
                                    value="2" 
                                  /> <strong>Unit Option 1:</strong> Sit Sollicitudin Cursus Parturient Purus <span>$24</span><br>
                                </label>
                                <label class="checkbox">
                                  <input type="checkbox" name="some_field" value="3" /> <strong>Unit Option 2:</strong> Sit Sollicitudin Cursus Parturient Purus <span>$32</span><br>
                                </label>
                                <label class="checkbox">
                                  <input type="checkbox" name="some_field" value="6" />  <strong>Unit Option 3:</strong> Sit Sollicitudin Cursus Parturient Purus <span>$26</span><br>
                                </label>
                                </p>
                                <button type="submit" id="register" class="orange button register">Purchase</button>
                                <p class="help-block"></p>
                              </div>
                            </div>
                          </form>
                          </div>
                           
                           <!-- RESULT AFTER USER CLICKS REGISTER
                           <div class="reg-result">Thank you for registering for this event</div>
                            -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- END MODAL -->
                </div> 
              </li>
              <li class="dash_container col-xs-12 col-sm-3-5 col-md-3-5 options_wrp_NoMargin">
                <h5>Coaching</h5>
                <div class="info">
                  <p>Discription od this option lorem ipsum dolor sit amet consectucar.</p>
                  <div class="regBtn">
                    <!-- Button trigger modal -->
                    <button type="button" id="register" class="orange button register" data-toggle="modal" data-target="#myModal">SEE MORE</button>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Name of the option</h4>
                        </div>
                        <div class="modal-body">
                          <div class="description">
                          <p><strong>Program Description:</strong> Lorem ipsum dolor sit amet, sed ut oblique tractatos. Ullum dissentias vel ea. Dicta nulla solet eum te, ut mea aeque nemore maluisset. </p>
                          <form>
                            <p>
                          <input type="radio" name="r-btn" value="r-btn1"> <strong>Unit Option 1:</strong>: Sit Sollicitudin Cursus Parturient Purus <span>$24</span><br>
                          <input type="radio" name="r-btn" value="r-btn2"> <strong>Unit Option 2:</strong>: Sit Sollicitudin Cursus Parturient Purus <span>$32</span><br>
                          <input type="radio" name="r-btn" value="r-btn3"> <strong>Unit Option 3:</strong>: Sit Sollicitudin Cursus Parturient Purus <span>$26</span><br>
                          </p>
                          </form>
                          </div>
                           <button type="button" id="register" class="orange button register">Register</button>
                           <!-- RESULT AFTER USER CLICKS REGISTER
                           <div class="reg-result">Thank you for registering for this event</div>
                            -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- END MODAL -->
                </div> 
              </li>
            </ul>
            <div class="holder" id="holder2"></div>
          </div><!-- END ROW -->
          <!-- END IN OFFICE EVENTS --> 
          
        </div><!--END DASH WRP -->
      </div><!-- END CONTAINER WRAP -->

      <footer class="dashfooter navbar hidden-xs">
      <!-- INCLUDE FOOTER IN FINAL CODE  < ?php include 'includes/footer.php' ;?> -->
        <div class="inner-footer">
          <div class="footer-logo">
            <img id="ft-logo" src="images/logo.png" alt="Right Mangement Logo" />
          </div>
          <ul class="footerNav"> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </footer> 
    </div><!-- END MAIN WRAPPER --> 

    
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jqBootstrapValidation.js"></script>
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   

      <script src="js/modernizr.custom.js"></script>
      <script src="js/jPages.js"></script>
     
    <script>
    // RESIZE FIX //
        $(function(){
        var windowH = $(window).height();
        var wrapperH = $('.dash_wrp').height()+177;
        var navHeignt = $('#orange_icon').height()+77;
        if(windowH < navHeignt && wrapperH < navHeignt) {                            
            $('#wrapper').css('height', (navHeignt)+'px');
            $('.dashboard').css('margin-bottom', '-86px');
        } 
        else if(windowH > wrapperH) {                            
            $('#wrapper').css({'height':($(window).height()-85)+'px'});
            $('.dashboard').css('margin-bottom', '-86px');
        } 
        

        function windowResizer(){
            var windowH = $(window).height();
            var wrapperH = $('.dash_wrp').height()+177;
            var differenceH = windowH - wrapperH;
            var newH = wrapperH + differenceH;
            var truecontentH = $('.dash_wrp').height()+177;
            var navHeignt = $('#orange_icon').height()+77;
            if(windowH < navHeignt && wrapperH < navHeignt) {
                $('#wrapper').css('height', (navHeignt)+'px');
                 $('.dashboard').css('margin-bottom', '-86px');
                console.log("windowResizer Worked 1")
            }
            else if(windowH > truecontentH) {
                $('#wrapper').css('height', (newH)-86+'px');
                 $('.dashboard').css('margin-bottom', '-86px');
                 console.log("windowResizer Worked 2")
        } }
                                                                                       
        $(window).resize(function(){
             windowResizer()

          })
        // END RESIZE FIX    //


        // SVG LOGO DETECT // 
        if (Modernizr.svg)
        {
          $("#logo").attr("src", "images/right_logo.svg");
          console.log('changed');

          $("#ft-logo").attr("src", "images/logo.svg");
          console.log('changed');
        }; 


        // MOBILE SIDE MENU TOGGLE // 
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

         $("#close-menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").removeClass("toggled");
        });
        
        // HIDES MOBILE SUB MENU //
        $(".carrer_coach").click(function(e) {
            e.preventDefault();
            $("#coach-info").toggleClass("invisible");
        });

        $(".help").click(function(e) {
            e.preventDefault();
            $("#help-info").toggleClass("invisible");
        });

        
             
    });
        
     </script>
    
    

  </body>
  </html>
