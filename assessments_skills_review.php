<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Manpower RightEverywhere </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/icon-style.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="resumePage" class="dashboard">
    <!-- START HEADER -->
    <div class="header_wrp navbar navbar-default"  role="navigation">
      <a  href="#menu-toggle" id="menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </a>
      <div class="dash_logo">
        <img id="logo" src="images/right_logo.png" alt="Right Mangement Logo" />
      </div>
      
      <ul class="header_nav hidden-xs">
        <li><a href="#" >Resources</a></li>
        <li><a href="#">Log Out</a></li>
      </ul>
      <a href="#" class="hidden-xs" id="messages">
        <span>3</span>
      </a>
    </div>
    <!-- END HEADER -->
 
    <!-- START WRAPPER -->
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
      <!-- SIDEBAR MENU START -->
      <div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
        <a href="#clost-menu-toggle" class="close-toggle" id="close-menu-toggle">X</a>
        <ul class="nav top-nav hidden-sm hidden-md hidden-lg">
          <li><a class="noICO" href="#">NOTIFICATIONS</a></li>
          <li><a class="noICO" href="#">RESOURCES</a></li>
          <li><a class="noICO" href="#">LOG OUT</a>
        </ul>
        <ul class="nav orange-icon" id="orange_icon">
          <li><div class="navTitle">MAIN</div></li>
          <li><a href="#"><span class="icon-dashboard-icon icon-md nav-color" aria-hidden="true"></span>Dashboard</a></li>
          <li><a href="#"><span class="icon-profile-icon icon-md nav-color" aria-hidden="true"></span>Profile</a></li>
          <li><a class="carrer_coach" href="#"><span class="icon-career-coach-icon icon-md nav-color" aria-hidden="true"></span>Career Coach</a>
            <ul>
              <li id="coach-info">
                <h4>Han Solo</h4>
                <div class="phone">123-123-1234</div> 
                <div class="email">hansolo@galaxy.com</div>
              </li>
            </ul>
          </li>
          <li><a href="#"><span class="icon-events-icon icon-md nav-color" aria-hidden="true"></span>Events &amp; Webinars</a></li>
          <li><a href="#"><span class="icon-favorite-icon icon-md nav-color" aria-hidden="true"></span>Favorites</a></li> 
          <li><a class="help" href="#"><span class="icon-help-icon icon-md nav-color" aria-hidden="true"></span>Help</a>
            <ul>
              <li id="help-info" class="help-desk">
                <h4>Help Desk</h4>
                <div class="p2">
                  <p>For assistance with RightEverywhere technical questions and quick job search coaching questions, please call <strong>800.668.8556</strong> </p>
                  <p>8AM- 8PM Eastern (Mon - Fri) <br>9AM- 5PM Eastern (Sat - Sun)</p>
                </div>
              </li>
            </ul>
          </li>
          <li><div class="navTitle">PROGRESS</div></li>
          <li><a href="#"><span class="icon-discovery-icon icon-md nav-color" aria-hidden="true"></span>Self Discovery</a></li>
          <li><a href="#"><span class="icon-career-planning-icon icon-md nav-color" aria-hidden="true"></span>Career Planning</a></li>
          <li><a href="#"><span class="icon-personal-branding-icon icon-md nav-color" aria-hidden="true"></span>Personal Branding</a></li>
          <li><a href="#"><span class="icon-job-search-icon icon-md nav-color" aria-hidden="true"></span>Job Search</a></li>
          <li><a href="#"><span class="icon-close-deal-icon icon-md nav-color" aria-hidden="true"></span>Close the Deal</a></li>             
        </ul>
        <div class="sb-footer navbar hidden-sm hidden-md hidden-lg">
          <ul class="footerNav "> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </div>
      <!-- SIDEBAR MENU END -->
      
      <!-- START PAGE CONTAINER -->
      <div class="container_wrp" id="container_wrp">
        <div class="dash_wrp">
          <div class="row">
            <a href="#" class="left backBTN"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Back To Self Discovery</a>
            
            <!-- CONTAINER --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <div class="p2">
                <h4 class="resource blue_txt">Identifying Skills: Review</h4>
                <div class="right col-xs-12 col-sm-12 col-md-12">
                    <p>
                     Use this to better understand your interests and careers related to them. Quisque porttitor pulvinar diam id mattis. Pellentesque lacinia laoreet mi, at sodales leo volutpat at. Vivamus ex sapien, sagittis eu diam non, congue lobortis tortor.
                    </p>
                </div>
              </div>
            </div><!-- END CONTAINER--> 

            <!-- CONTAINER --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <div class="p2">
                <h3 style="color: #4f9ace;">High Skill</h3>
                <div id="skills">
					<div class="skillview" style="width: 95%;">people development</div>
					<div class="skillview" style="width: 90%;">creative/conceptual</div>
					<div class="skillview" style="width: 85%;">project management</div>
					<div class="skillview" style="width: 80%;">skill 1</div>
					<div class="skillview" style="width: 75%;">skill 2</div>
					<div class="skillview" style="width: 70%;">skill 3</div>
					<div class="skillview" style="width: 65%;">skill 4</div>
					<div class="skillview" style="width: 60%;">skill 5</div>
					<div class="skillview" style="width: 55%;">skill 6</div>
					<div class="skillview" style="width: 50%;">skill 7</div>
					<div class="skillview" style="width: 55%;">skill 6</div>
					<div class="skillview" style="width: 55%;">skill 6</div>
					<div class="skillview" style="width: 55%;">skill 6</div>
					<div class="skillview" style="width: 55%;">skill 6</div>
					<div class="skillview" style="width: 55%;">skill 6</div>


              </div>
              <h3 style="color: #5aa88a;">Low Skill</h3>
              </div>
            </div><!-- END CONTAINER--> 


             <!-- CONTAINER --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
                <h5 class="lt_gray_txt">CAREERS THE MATCH YOUR SKILLS</h5>
                <div class="p2">
					<div class=" col-xs-12 col-sm-4 col-md-4">
						Account Executive<br />
						Financial Advisor<br />
						Risk Assessor
					</div>
					<div class=" col-xs-12 col-sm-4 col-md-4">
						Creative Director<br />
						Retail Manager
					</div>      

                </div>
            </div><!-- END CONTAINER-->


             <!-- START  --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
             
              <div class="p2">
                 <div class=" col-xs-12 col-sm-4 col-md-4">
                <a class="left mb2-0" href="#"><span class="icon-share-icon icon-md left" aria-hidden="true"></span><span class="lh2-2">Share With Coach</span></a>
              </div>
              <div class=" col-xs-12 col-sm-4 col-md-4">
                <a class="left mb2-0" href="#"><span class="icon-download-icon icon-md left" aria-hidden="true"></span><span class="lh2-2">Download as PDF</span></a>
              </div>
          
                <div class="right col-xs-12 col-sm-12 col-md-12">
                  <div class="col-xs-12 col-sm-12 col-lg-12 p0"> 
                   <a class="left black ls1 mb2-0" href="#"><span class="icon-back-to-icon icon-md left" aria-hidden="true"></span><span class="lh2-2">BACK TO SELF DISCOVERY</span></a>
                   <a class="orange button right next mb2" id="next2"  href="resume_builder_step7.php">NEXT ASSESSMENT</a> 
                  </div>
                   
                </div>
              </div>
            </div><!-- END --> 
             
          </div><!-- END ROW -->
        </div><!-- END DASH WRAP -->
      </div><!-- END PAGE CONTAINER -->     
        
      <footer class="dashfooter navbar hidden-xs">
      <!-- INCLUDE FOOTER IN FINAL CODE  < ?php include 'includes/footer.php' ;?> -->
        <div class="inner-footer">
          <div class="footer-logo">
            <img id="ft-logo" src="images/logo.png" alt="Right Mangement Logo" />
          </div>
          <ul class="footerNav"> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </footer> 
    </div><!-- END MAIN WRAPPER --> 
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   

      <script src="js/modernizr.custom.js"></script>
     
    <script>
       // SVG LOGO DETECT // 
        if (Modernizr.svg)
        {
          $("#logo").attr("src", "images/right_logo.svg");
          console.log('changed');

          $("#ft-logo").attr("src", "images/logo.svg");
          console.log('changed');
        }; 

       

        // MOBILE SIDE MENU TOGGLE // 
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

         $("#close-menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").removeClass("toggled");
        });
        
        // HIDES MOBILE SUB MENU //
        $(".carrer_coach").click(function(e) {
            e.preventDefault();
            $("#coach-info").toggleClass("invisible");
        });

        $(".help").click(function(e) {
            e.preventDefault();
            $("#help-info").toggleClass("invisible");
        });

        
     </script>
    


<script>
function makeGradientColor(color1, color2, percent) {
    var newColor = {};

    function makeChannel(a, b) {
        return(a + Math.round((b-a)*(percent/100)));
    }

    function makeColorPiece(num) {
        num = Math.min(num, 255);   // not more than 255
        num = Math.max(num, 0);     // not less than 0
        var str = num.toString(16);
        if (str.length < 2) {
            str = "0" + str;
        }
        return(str);
    }

    newColor.r = makeChannel(color1.r, color2.r);
    newColor.g = makeChannel(color1.g, color2.g);
    newColor.b = makeChannel(color1.b, color2.b);
    newColor.cssColor = "#" + 
                        makeColorPiece(newColor.r) + 
                        makeColorPiece(newColor.g) + 
                        makeColorPiece(newColor.b);
    return(newColor);
}

var green = {r:90, g:168, b:138};
var blue = {r:79, g:154, b:206};
var $j_object = $(".skillview");
var steps = $($j_object).length - 2;
var stepchange = 100/steps;
var stepper = 100;
$j_object.each( function(i) 
{ 

	stepper = stepper - stepchange;
	if (i == 0){ $(this).css('background-color', '#4f9ace');}
	
	else if (i == ($($j_object).length)-1){$(this).css('background-color', '#5aa88a');}
	
	else {
	var newColor = makeGradientColor(green, blue, stepper);
	$(this).css('background-color', newColor.cssColor);
	}


});


</script>


  </body>
</html>
