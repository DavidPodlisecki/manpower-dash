<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Manpower RightEverywhere </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/icon-style.css" rel="stylesheet">
    <link type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet" />  
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
   
    
  </head>
  <body id="cardsortPage" class="dashboard">
    <!-- START HEADER -->
    <div class="header_wrp navbar navbar-default"  role="navigation">
      <a  href="#menu-toggle" id="menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </a>
      <div class="dash_logo">
        <img id="logo" src="images/logo.png" alt="Right Mangement Logo" />
      </div>
      
      <ul class="header_nav hidden-xs">
        <li><a href="#" >Resources</a></li>
        <li><a href="#">Log Out</a></li>
      </ul>
      <a href="#" class="hidden-xs" id="messages">
        <span>3</span>
      </a>
    </div>
    <!-- END HEADER -->
 
    <!-- START WRAPPER -->
    <div id="wrapper" class="row row-offcanvas row-offcanvas-left">
      <!-- SIDEBAR MENU START -->
      <div id="sidebar-wrapper" class="sb-column col-xs-3 col-sm-3 sidebar-offcanvas" role="navigation">
        <a href="#clost-menu-toggle" class="close-toggle" id="close-menu-toggle">X</a>
        <ul class="nav top-nav hidden-sm hidden-md hidden-lg">
          <li><a class="noICO" href="#">NOTIFICATIONS</a></li>
          <li><a class="noICO" href="#">RESOURCES</a></li>
          <li><a class="noICO" href="#">LOG OUT</a>
        </ul>
        <ul class="nav orange-icon" id="orange_icon">
          <li><div class="navTitle">MAIN</div></li>
          <li><a href="#"><span class="icon-dashboard-icon icon-md nav-color" aria-hidden="true"></span>Dashboard</a></li>
          <li><a href="#"><span class="icon-profile-icon icon-md nav-color" aria-hidden="true"></span>Profile</a></li>
          <li><a class="carrer_coach" href="#"><span class="icon-career-coach-icon icon-md nav-color" aria-hidden="true"></span>Career Coach</a>
            <ul>
              <li id="coach-info">
                <h4>Han Solo</h4>
                <div class="phone">123-123-1234</div> 
                <div class="email">hansolo@galaxy.com</div>
              </li>
            </ul>
          </li>
          <li><a href="#"><span class="icon-events-icon icon-md nav-color" aria-hidden="true"></span>Events &amp; Webinars</a></li>
          <li><a href="#"><span class="icon-favorite-icon icon-md nav-color" aria-hidden="true"></span>Favorites</a></li> 
          <li><a class="help" href="#"><span class="icon-help-icon icon-md nav-color" aria-hidden="true"></span>Help</a>
            <ul>
              <li id="help-info" class="help-desk">
                <h4>Help Desk</h4>
                <div class="p2">
                  <p>For assistance with RightEverywhere technical questions and quick job search coaching questions, please call <strong>800.668.8556</strong> </p>
                  <p>8AM- 8PM Eastern (Mon - Fri) <br>9AM- 5PM Eastern (Sat - Sun)</p>
                </div>
              </li>
            </ul>
          </li>
          <li><div class="navTitle">PROGRESS</div></li>
          <li><a href="#"><span class="icon-discovery-icon icon-md nav-color" aria-hidden="true"></span>Self Discovery</a></li>
          <li><a href="#"><span class="icon-career-planning-icon icon-md nav-color" aria-hidden="true"></span>Career Planning</a></li>
          <li><a href="#"><span class="icon-personal-branding-icon icon-md nav-color" aria-hidden="true"></span>Personal Branding</a></li>
          <li><a href="#"><span class="icon-job-search-icon icon-md nav-color" aria-hidden="true"></span>Job Search</a></li>
          <li><a href="#"><span class="icon-close-deal-icon icon-md nav-color" aria-hidden="true"></span>Close the Deal</a></li>             
        </ul>
        <div class="sb-footer navbar hidden-sm hidden-md hidden-lg">
          <ul class="footerNav "> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </div>
      <!-- SIDEBAR MENU END -->
      
      <!-- START PAGE CONTAINER -->
      <div class="container_wrp" id="container_wrp">
        <div class="dash_wrp">
          <div class="row">
            <a href="#" class="left backBTN"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Back To Self Discovery</a>
            
            <!-- START INTRO --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
              <div class="p2">
                <h4 class="resource blue_txt">Values And Drivers</h4>
                <div class="right col-xs-12 col-sm-12 col-md-12">
                  <div class="row">
                    <h6 class="resource_list_title">Instructions</h6>
                    <p>
                      <ul>
                        <li>Click and drag each card from the deck on the left and drop directly onto the top of the appropriate pile. </li>
                        <li>Add your own card: If your values and drivers are not shown, you may add up to 5 values of your own.</li>
                        <li>Reset: Use the reset button at any time to start over. All results and created values will be lost.</li>
                        <li>Finish: When finished, lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                      </ul>
                    </p>
                  
                  </div> 
                  
                  <a class="green button right mb2" href="#">EXIT</a><a class="blue button right mb2" href="#">SAVE</a> 
                   
                </div>
              </div>
            </div><!-- END INTRO --> 

          <!-- START CARD SORT  -->
           
            <div class="row">
              <div class="dash_container panel panel-white card-wrp card-wrp-xs card-wrp-sm card-wrp-md card-wrp-lg">
                <div class="panel-heading">
                  <h3 class="panel-title">Cards Remaining:<span id="cardcount">24</span></h3>
                </div>
                
				<ul class="draggable droppable" id="deck" >
				
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome,  or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>

								<li class="card"><!-- CARD -->
									<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>

				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>
					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents.</p>
					</div>
					</div>
				<!-- CARD END --> </li>
				<li class="card"><!-- CARD -->
					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title">Challenge</h3>

					</div>
					<div class="p1">
					  <p class="center">Work which requires me to overcome, solve difficult problems or win over extremely tough opponents. </p>
					</div>
					</div>
				<!-- CARD END --> </li>	

   				<li id="usercard1" class="card"><!-- usercard1 -->
   					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title ">Title</h3> 
					</div>
					<div class="p1">
					  <p class="center">Thisis where the copy goes</p>
					  <button type="button" id="close-card1"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> DELETE CARD</button>
					</div>
					</div>
				<!-- usercard1 --> </li>   
   
   				<li id="usercard2" class="card"><!-- usercard2 -->
   					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title ">Title</h3>
					</div>
					<div class="p1">
					  <p class="center">Thisis where the copy goes</p>
					  <button type="button" id="close-card2"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> DELETE CARD</button>
					</div>
					</div>
				<!-- usercard2 --> </li>   
				
   				<li id="usercard3" class="card"><!-- usercard3 -->
   					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title ">Title</h3>
					</div>
					<div class="p1">
					  <p class="center">Thisis where the copy goes</p>
					  <button type="button" id="close-card3"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> DELETE CARD</button>
					</div>
					</div>
				<!-- usercard3 --> </li>   				

   				<li id="usercard4" class="card"><!-- usercard4 -->
   					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title ">Title</h3>
					</div>
					<div class="p1">
					  <p class="center">Thisis where the copy goes</p>
					  <button type="button" id="close-card4"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> DELETE CARD</button>
					</div>
					</div>
				<!-- usercard4 --> </li>   
				
   				<li id="usercard5" class="card"><!-- usercard5 -->
   					<span class="glyphicon glyphicon-collapse-down expand" aria-hidden="true"></span>
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading">
					  <h3 class="panel-title ">Title</h3>
					</div>
					<div class="p1">
					  <p class="center">Thisis where the copy goes</p>
					  <button type="button" id="close-card5"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> DELETE CARD</button>
					</div>
					</div>
				<!-- usercard5 --> </li>   				

                
				</ul>

              </div>
              <div class="dash_container panel panel-blue card-wrp card-wrp-xs card-wrp-sm card-wrp-md card-wrp-lg">
                <div class="panel-heading">
                  <h3 class="panel-title">ESSENTIAL</h3>
                </div>
				<ul class="droppable draggable cardlist"></ul>
              </div>
              <div class="dash_container panel panel-orange card-wrp card-wrp-xs card-wrp-sm card-wrp-md card-wrp-lg">
                <div class="panel-heading">
                  <h3 class="panel-title">Important</h3>
                </div>
				<ul class="draggable droppable cardlist"></ul>
              </div>
              <div class="dash_container panel panel-green card-wrp card-wrp-xs card-wrp-sm card-wrp-md card-wrp-lg ">
                <div class="panel-heading">
                  <h3 class="panel-title">Nice To Have</h3>
                </div>
				<ul class="draggable droppable cardlist"></ul>
              </div>
              <div class="dash_container panel panel-dkgray card-wrp card-wrp-xs card-wrp-sm card-wrp-md card-wrp-lg ">
                <div class="panel-heading">
                  <h3 class="panel-title">Not Important</h3>
                </div>
                <ul class="draggable droppable cardlist"></ul>
              </div>
              <div class="dash_container panel panel-red card-wrp card-wrp-xs card-wrp-sm card-wrp-md card-wrp-lg ">
                <div class="panel-heading">
                  <h3 class="panel-title">Avoid</h3>
                </div>
                <ul class="draggable droppable cardlist"></ul>
              </div>
            </div>
            
             
           
            <!-- END  CARD SORT-->

             <!-- START  --> 
            <div class="dash_container col-xs-12 col-sm-12 col-md-12">
             
              <div class="p2">
                
                <div class="col-xs-12 col-sm-12 col-md-5 col-centered">
                  <div class="col-xs-12 col-sm-12 col-lg-12 p0"> 
                   <button class="left ls1 mb2-0 no-style" href="#"><span class="icon-reset-icon icon-md left" aria-hidden="true"></span><span class="lh2-2 ls2" id="reset" >RESET</span></button>
                   <a class="orange button right next mb2" id="next2"  href="resume_builder_step7.php">FINISH</a> 
                  </div>
                   
                </div>
              </div>
            </div><!-- END --> 
            

          </div><!-- END ROW -->
        </div><!-- END DASH WRAP -->
      </div><!-- END PAGE CONTAINER -->     
        
      <footer class="dashfooter navbar hidden-xs">
      <!-- INCLUDE FOOTER IN FINAL CODE  < ?php include 'includes/footer.php' ;?> -->
        <div class="inner-footer">
          <div class="footer-logo">
            <img id="ft-logo" src="images/logo.png" alt="Right Mangement Logo" />
          </div>
          <ul class="footerNav"> 
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Your Data Privacy</a></li>
            <li><a href="#">Cookie Overview</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
          <div class="copyright">Copyright © 2014 Right Management. A wholly-owned subsidiary of ManpowerGroup All rights reserved.</div>
        </div>
      </footer> 
    </div><!-- END MAIN WRAPPER --> 
   
   
   
  				<li id="addcard"><!-- addCARD -->
					<div class="dash_container panel panel-ltgray">
					<div class="panel-heading add-title">
					  <h3 class="panel-title ">ADD YOUR OWN CARD</h3>
					</div>
					<div class="p1">
					  <span id="addlink"><a class="lmodal1" href="#editModal1" role="buton" data-toggle="modal">+</a></span>
					</div>
					</div>
				<!-- addCARD END --> </li> 
   
   
   
   


<!-- Modal HTML -->

                    <div id="editModal1" class="modal fade ">

                        <div class="modal-dialog" style="width:450px;">

                            <div class="modal-content">

                                <div class="modal-header">

                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                    <h6 class="modal-title">Add your own Card</h6>

                                </div>

                                <div class="modal-body">
									<form method="post" id="cardaddform">


									<div class="dash_container panel panel-ltgray">
									<div class="panel-heading">
									  <h3 class="panel-title"><input name="title" type="text" class="rd5" size="35" maxlength="35" /></h3>
									</div>
									<div class="p1">
									  <p class="center"><textarea class="full-width" maxlength="140" id="bodycopy" name="bodycopy" rows="4" cols="18"></textarea></p>
									</div>
									</div>                           


									</form>
								</div>







                                <div class="modal-footer center">

                                    <button type="button" class="orange_INV button mr1" data-dismiss="modal">Cancel</button>

                                    <button type="button" class="orange button ">ADD</button>

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--end modal -->    
   
   
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <script src="js/responsive-tabs.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   

      <script src="js/modernizr.custom.js"></script>
     
    <script type="text/javascript">
        // SVG LOGO DETECT // 
        if (Modernizr.svg)
        {
          $("#logo").attr("src", "images/right_logo.svg");
          console.log('changed');

          $("#ft-logo").attr("src", "images/logo.svg");
          console.log('changed');
        }; 

        // MOBILE SIDE MENU TOGGLE // 
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

         $("#close-menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").removeClass("toggled");
        });
        
        // HIDES MOBILE SUB MENU //
        $(".carrer_coach").click(function(e) {
            e.preventDefault();
            $("#coach-info").toggleClass("invisible");
        });

        $(".help").click(function(e) {
            e.preventDefault();
            $("#help-info").toggleClass("invisible");
        });

        
     </script>
 
 


  <script>
  equalheight = function(container){

	var currentTallest = 0,
	     currentRowStart = 0,
	     rowDivs = new Array(),
	     $el,
	     topPosition = 0;
	 $(container).each(function() {

	   $el = $(this);
	   $($el).height('auto')
	   topPostion = $el.position().top;

	   if (currentRowStart != topPostion) {
	     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
	       rowDivs[currentDiv].height(currentTallest);
	     }
	     rowDivs.length = 0; // empty the array
	     currentRowStart = topPostion;
	     currentTallest = $el.height();
	     rowDivs.push($el);
	   } else {
	     rowDivs.push($el);
	     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
	  }
	   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
	     rowDivs[currentDiv].height(currentTallest);
	   }
	 });
	}

	$(window).load(function() {
	  equalheight('.card-wrp');
	});


	$(window).resize(function(){
	  equalheight('.card-wrp');
	});

  $(function() {



    $( ".draggable li" ).draggable({ revert: "invalid" });

		$( "#usercard1" ).appendTo("body");
		$( "#usercard1" ).hide();
		$("#close-card1").click(function(){
         $( "#usercard1" ).appendTo("body");
		 $( "#usercard1" ).hide();
		 $( "#addcard" ).appendTo("#deck");
		 $( "#addcard" ).show();
		 usercounter--;
        });
		$( "#usercard2" ).appendTo("body");
		$( "#usercard2" ).hide();
		$("#close-card2").click(function(){
         $( "#usercard2" ).appendTo("body");
		 $( "#usercard2" ).hide();
		 $( "#addcard" ).appendTo("#deck");
		 $( "#addcard" ).show();
		 reMargify();
		 usercounter--;
		 
        });
		$( "#usercard3" ).appendTo("body");
		$( "#usercard3" ).hide();
		$("#close-card3").click(function(){
         $( "#usercard3" ).appendTo("body");
		 $( "#usercard3" ).hide();
		 $( "#addcard" ).appendTo("#deck");
		 $( "#addcard" ).show();
		 reMargify();
			 usercounter--;
	
        });
		$( "#usercard4" ).appendTo("body");
		$( "#usercard4" ).hide();
		$("#close-card4").click(function(){
         $( "#usercard4" ).appendTo("body");
		 $( "#usercard4" ).hide();
		 $( "#addcard" ).appendTo("#deck");
		 $( "#addcard" ).show();
		 reMargify();
			 usercounter--;
	 
        });
		$( "#usercard5" ).appendTo("body");
		$( "#usercard5" ).hide();
		$("#close-card5").click(function(){
         $( "#usercard5" ).appendTo("body");
		 $( "#usercard5" ).hide();
		 $( "#addcard" ).appendTo("#deck");
		 $( "#addcard" ).show();
		 reMargify();
		 usercounter--;
		 
        });
		var usercounter = 1;

    
    $( "ul.droppable" ).droppable({
     
      hoverClass: "ui-state-hover",
      accept: ":not(.ui-sortable-helper)",
      drop: function( event, ui ) {
        movethecard( ui.draggable, this);

        
      }
    });
 

    function movethecard( $item, $eltarget ) {
      $item.fadeOut(function() {
      
 
       
        $item.appendTo( $eltarget ).fadeIn(function(){checkthecount();});
        $item.css("left", 0);
        $item.css("top", 0);
        if ($('li').hasClass("highlight")) {
   			$(this).removeClass( "highlight" );
   			$(this).css('margin-bottom', '0');
  		}
         equalheight('.card-wrp');
         reMargify();
         deckMargify();
      });
      
    }
    

    $( "#reset" ).click(function () {
  
    var txt;
var r = confirm("Are you sure you want to reset?");
if (r == true) {
 		$( "#usercard1" ).appendTo("body");
		$( "#usercard1" ).hide();

		$( "#usercard2" ).appendTo("body");
		$( "#usercard2" ).hide();

		$( "#usercard3" ).appendTo("body");
		$( "#usercard3" ).hide();

		$( "#usercard4" ).appendTo("body");
		$( "#usercard4" ).hide();

		$( "#usercard5" ).appendTo("body");
		$( "#usercard5" ).hide();

		$( "#addcard" ).appendTo("body");
		$( "#addcard" ).hide();

         


		usercounter = 1;   
		$( ".droppable li" ).each(function( ) {
			$(this).fadeOut(0, function() {
				$(this).appendTo("#deck").fadeIn(function(){checkthecount();});
        		$(this).css("left", 0);
        		$(this).css("top", 0);
			});
		});

	$('.card-wrp').removeAttr('style');
    
    
    deckMargify();
    reMargify();
} else {

}
    

		
		
    });

$( "#reset" ).click(function () {equalheight('.card-wrp');});
    $( ".modal-footer .orange" ).click(function () {
    
		//alert('dude you added it');
				//$(this).appendTo("#deck").fadeIn(function(){checkthecount();};
				

		//#usercard1

	
		
		
		$("#usercard"+usercounter+" .panel-title").text($( "input:text[name=title]" ).val());
		$("#usercard"+usercounter+" .p1 p").text($( "#bodycopy" ).val());

			$( "#addcard" ).appendTo("body");
			$( "#addcard" ).hide();
		$("#usercard"+usercounter).appendTo("#deck").fadeIn(function(){checkthecount();});

		usercounter++;
		

			
		$( ".modal-footer .orange_INV" ).trigger( "click" );
		
		$( "#bodycopy" ).val('');
		$( "input:text[name=title]" ).val('');
		 deckMargify();
    });

   



    // CARD MARGIN //
    function deckMargify(){
    $('ul#deck').each(function(){
		$(this).find('li').each(function(){
			var newarginTop = -1*($(this).prev().height()-10);
			$(this).css('margin-top', newarginTop);

		});
		});
	}
	deckMargify();

	function reMargify(){	
	$('ul.cardlist').each(function(){
		$(this).find('li').each(function(){
			var newMarginTop = -1*($(this).prev().height()-30);
			$(this).css('margin-top', newMarginTop);
		});
		});
	}

	reMargify();

	  $(window).resize(function() {
	        
	   reMargify();
	   deckMargify();
		 });

	// END CARD MARGIN //


//	$( ".panel-heading" ).toggle(function () {
//	$('ul.cardlist').each(function(){
//		$(this).find('li').each(function(){
//			var revealCard = $(this).height();
//			$(this).css('margin-bottom', revealCard);
//		});
//		});
//	})

//	$( ".panel-heading" ).toggle(
  //function() {
   // $( this ).addClass( "selected" );
 // }, function() {
  //  $( this ).removeClass( "selected" );
  //}
//);	

	//$('.panel-heading').each(function(){
	//      var parentHeight = $(this).parent().height();
	//      $(this).height(parentHeight);    
	//});
$( "li span.expand" ).click(function() {
  if ($(this).parent().hasClass("highlight")) {
   $(this).parent().removeClass( "highlight" );
   $(this).parent().css('margin-bottom', '0');
  }
  else if ($(this).parent().is(":last-child")) {
   $(this).parent().css('margin-bottom', '0');
  }
  else if($(this).parent()){
  var margBottom = 1*($(this).parent().height()-30);
  $(this).parent().css('margin-bottom', margBottom);
  $(this).parent().addClass( "highlight" );
  }
  
  equalheight('.card-wrp');
});



	function checkthecount(){
		var cardcounttotal = $("#deck li.card").length;
		$("#cardcount").text(cardcounttotal);
		if (cardcounttotal == 0) 
		{
			if (usercounter <=5)
			{
			$( "#addcard" ).appendTo("#deck");
			$( "#addcard" ).show();
			}
		}
		
	
    }
  });
  </script>



  </body>
</html>
