
// LOGO FALLBACK - SVG LOGO DETECT
   if (Modernizr.svg)
   {
     $("#logo").attr("src", "images/right_logo.svg");
     console.log('changed');

     $("#ft-logo").attr("src", "images/logo.svg");
     console.log('changed');
   };

   // MOBILE SIDE MENU TOGGLE //
   $("#menu-toggle").click(function(e) {
       e.preventDefault();
       $("#wrapper").toggleClass("toggled");
   });

    $("#close-menu-toggle").click(function(e) {
       e.preventDefault();
       $("#wrapper").removeClass("toggled");
   });

   // HIDES MOBILE SUB MENU //
   $(".carrer_coach").click(function(e) {
       e.preventDefault();
       $("#coach-info").toggleClass("invisible");
   });

   $(".help").click(function(e) {
       e.preventDefault();
       $("#help-info").toggleClass("invisible");
   });
